--[[
********************************************************************************
	Project owner:		RageQuit community
	Project name: 		TopCity-RP
	Developers:   		Mr_Moose

	Source code:		https://github.com/404rq/TopCity-RP/
	Bugtracker: 		https://discuss.404rq.com/t/issues
	Suggestions:		https://discuss.404rq.com/t/development

	Version:    		Open source
	License:    		BSD 2-Clause
	Status:     		Stable release
********************************************************************************
]]--

--[[ Dispaly a DX topbar message ]]--
function dm(text, plr, r,g,b, col, bell)
	if col == nil then col = false end
	if bell then playSoundFrontEnd(plr, 11) end
	if not plr or not isElement(plr) or getElementType(plr) ~= "player" then return end
	triggerClientEvent(plr, "topbar.addText", root, text, r,g,b, col)
end

--[[ Display basic information ]]--
addCommandHandler("gtainfo", function(plr, cmd)
	outputChatBox("[TopCity-RP] "..getResourceName(
	getThisResource())..", by: "..getResourceInfo(
        getThisResource(), "author")..", v-"..getResourceInfo(
        getThisResource(), "version")..", is represented", plr)
end)
