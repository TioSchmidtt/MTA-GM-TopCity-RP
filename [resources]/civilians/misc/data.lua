--[[
	BELOW INFORMATION ARE PROPERTY OF GRAND THEFT WALRUS RPG,
	IT'S SHARED HERE JUST TO SHOW THE LAYOUT STRUCTURE OF THE
	TABLE AND SHOULD NOT BE USED DIRECTLY IN OTHER SERVERS.

	ALSO NOTE THAT THIS IS JUST THE JOB INFORMATION TO ASSIGN
	TO A PLAYER AND NOT THE ACTUAL JOB FUNCTIONALITY. JOB
	TEMPLATES INCLUDED IN THIS PROJECT MAY BE USED IN THEIR
	CURRENT SHAPE, THIS INCLUDES FARMER AND FISHER.
]]--

work_items = {
	-- Index: Name, Team, MaxWl, Description, Skins (Table)
	["Bus Driver"]={ "Civilians", 1, [[
Como um Motorista de ônibus, você precisa pegar um ônibus ou van
para iniciar sua rota, você pode usar o seu próprio ou
Alugue um do marcador de geração próximo do veículo.

Você receberá o pagamento em torno de RR$70 / parada multiplicada
pelo seu nível atual, (veja F5 para descobrir
sobre o seu nível). Você também ganhará RR$5 / minuto
para cada passageiro que você pegar.

Armas:
- Desert eagle (5x)
- Baseball bat
	]], { -1, 60, 171, 172, 194, 253, 255 },
	{ "default", "Chinese casual dressed man", "White well dressed man", "White well dressed girl (blonde)", "White well dressed girl (brunette)", "Black man with uniform", "White man with mustache" },
	--[[WeaponID, amount, price, name]]
	{{5, 1, 40, "Baseball bat"}, {24, 35, 500, "Desert eagle"}},
	--[[Message for new players when applying for the job]]
	"Welcome to the bus driver job!, get a bus or coach to start your work"},
	["Train Driver"]={ "Civilians", 0, [[
Dirija o seu trem em torno das linhas, diminua a velocidade
abaixo de 5 km / h para parar na estação e obter
pagsmento, não dirija rápido (64 km / h na cidade
ou 100 km / h país), ou você pode descarrilar e
ser demitido dentro de um segundo.

Obtenha um trem de passageiros ou de carga nas proximidades
Desovador, Streak é um trem de passageiros e frete
um trem de carga obviamente. A quantidade de carros
pode ser fechado, mais carros tornam seu trem mais lento
mas você ganha mais dinheiro, um limite máximo é definido
e depende da sua classificação.

Você receberá o pagamento em torno de RR$250 / estação multiplicada
pelo seu nível atual (veja F5 para saber mais sobre
seu nivel). Você também ganhará RR$20 / minuto para
cada passageiro que você pegar.

Armas:
- Desert eagle (3x)
- Baseball bat
	]], { -1, 60, 171, 172, 194, 253, 255 },
	{ "default", "Chinese casual dressed man", "White well dressed man", "White well dressed girl (blonde)", "White well dressed girl (brunette)", "Black man with uniform", "White man with mustache" },
	--[[WeaponID, amount, price, name]]
	{{5, 1, 40, "Baseball bat"}, {24, 21, 300, "Desert eagle"}},
	--[[Message for new players when applying for the job]]
	"Welcome to the train driver job!, get a streak or freight to start your work"},
	["Taxi Driver"]={ "Civilians", 2, [[
Neste trabalho você precisa de um táxi, taxista ou limusine.
Obtenha-o de um gerador próximo ou use o seu próprio
veículo. Recolher e soltar os passageiros em
determinado destino.

Você receberá o pagamento até R $ 500 / trabalho dentro do mesmo
cidade e até R $ 1100 / trabalho para entregas de cidades cruzadas.
Você também ganhará R $ 30 / minuto por cada passageiro que
você pegou.

Armas:
- Pistol (4x)
- Knife
	]], { -1, 9, 14, 41, 44, 60, 72 },
	{ "default", "Casual African girl", "Casual african man", "Casual latino girl", "Casual latino man", "Chinese casual dressed man", "White casual dressed man" },
	--[[WeaponID, amount, price, name]]
	{{5, 1, 40, "Baseball bat"}, {22, 68, 200, "Pistol"}},
	--[[Message for new players when applying for the job]]
	"Welcome to the taxi driver job!, get a taxi or cabbie to start your work"},
	["Trucker"]={ "Civilians", 1, [[
Alugue ou use seu próprio caminhão ou caminhão semi, neste
O trabalho que você vai entregar carga em torno de San Andreas.
Os camionistas experientes podem escolher uma missão com
Dois trailers anexados.

Você receberá o pagamento até R $ 1800 / cargo multiplicado pelo seu
nível atual (veja F5 para descobrir o seu nível).
Seu pagamento depende da distância, da sua classificação e da
peso da sua carga.

Armas:
- Desert eagle (2x)
- Baseball bat
	]], { -1, 31, 112, 133, 172, 194, 198, 202 },
	{ "default", "White country girl", "Russian trucker", "Trucker with red hat", "White well dressed girl (blonde)", "White well dressed girl (brunette)", "Trucker girl", "Beer trucker" },
	--[[WeaponID, amount, price, name]]
	{{5, 1, 40, "Baseball bat"}, {24, 14, 200, "Desert eagle"}},
	--[[Message for new players when applying for the job]]
	"Welcome to the trucker job!, get a truck to start your work"},
	["Pilot"]={ "Civilians", 0, [[
Alugue ou use seu próprio avião ou helicóptero,
neste trabalho você vai pilotar  ao redor
San Andreas. As aeronaves grandes são limitadas a
pilotos experientes.

Você receberá o pagamento de R $ 1500 / emprego
Você também ganhará R $ 80 / minuto por cada passageiro
você pegou.

Armas:
- Pistol (3x)
- Nightstick
	]], { -1, 61, 71 },
	{ "default", "Pilot (official)", "Security guard" },
	--[[WeaponID, amount, price, name]]
	{{3, 1, 0, "Nightstick"}, {22, 51, 200, "Pistol"}},
	--[[Message for new players when applying for the job]]
	"Welcome to the pilot job!, get a plane or helicopter to start your work"},
	["Mechanic"]={ "Civilians", 4, [[
Responda às chamadas de jogadorese 
repare os veiculos quebrados ou
 para reabastecer veículos que tenha
ficando sem combustível.

Você receberá R $ 750 / reparo e R $ 500 / reabastecimento
(Você pode usar uma arma para ameaçar seus clientes
para pagar mais se quiser, note que você pode obter
queria por isso).

Armas:
- AK-47 (4x)
- Baseball bat
	]], { 50, 268, 305, 309 },
	{ "Mechanic (official)", "Dwaine", "Jethro", "Janitor" },
	--[[WeaponID, amount, price, name]]
	{{5, 1, 40, "Baseball bat"}, {30, 30, 700, "AK-47"}},
	--[[Message for new players when applying for the job]]
	"Welcome to the mechanic job!, press X and click on a damaged vehicle to fix or refuel"},
	["Fisher"]={ "Civilians", 5, [[
Você precisa possuir um barco para realizar este trabalho, obter
um barco, pegue o emprego e comece a pescar, fácil
Hã. Você pode usar sua espingarda para manter outro
pescadores, mas lembre-se de manter um olho de
unidades de lei próximas, uma vez que é ilegal;)

Você receberá R $ 100 / peixe
Seu pagamento pode ser melhor ou pior dependendo
com a frequência com que o peixe morde, isso muda
tempo e você pode ver o status atual do mercado 
de peixes use /mpesca a qualquer momento.

Armas:
- Shotgun (x20 bullets)
- Golf club
	]], { -1 },
	{ "default" },
	--[[WeaponID, amount, price, name]]
	{{2, 1, 60, "Golf club"}, {25, 20, 1400, "Shotgun"}},
	--[[Message for new players when applying for the job]]
	"Welcome to the fisher job!, enter a boat and wait to start catching fish"},
	["Farmer"]={ "Civilians", 5, [[
Você precisa de um trator e uma colheitadeira para este trabalho
Ou seja seu próprio ou um carrinho de mão ou de seu amigo.
Use o trator para plantar sementes em qualquer lugar do mapa,
Deixe crescer e, em seguida, você pode usar uma colheita para
fazer bolas fora das plantas, pegar as bolas para
ser pago.

Observe que outros agricultores podem roubar suas plantas para
Escolha um bom local e não planifique muito, a menos que você
tem controle total sobre seus campos, você pode matar
thiefs com apenas 25% do nível desejado se você
use um rifle de país.

Você receberá R $ 600 /planta - (R $ 100 para a semente)
Isto é tudo sobre timing, se você não é rápido o suficiente
suas colheitas podem se apodrecer, outros jogadores podem roubar sua
fardos, etc. O planejamento inteligente é a chave do sucesso.

Armas:
- Country rifle (x30 bullets)
- Desert eagle (x7)
- Baseball bat
	]], { -1, 157, 158, 159, 160, 161, 162 },
	{ "default", "Hillbilly girl", "Farmer man", "Hillbilly boy", "Farmer man (old)", "Farmer (official)", "Hillbilly (probably inbred)" },
	--[[WeaponID, amount, price, name]]
	{{5, 1, 40, "Baseball bat"}, {24, 49, 700, "Desert eagle"}, {33, 30, 3200, "Country rifle"}},
	--[[Message for new players when applying for the job]]
	"Welcome to the farmer job!, get a tractor and press N to buy and plant seeds"},
	["Tram Driver"]={ "Civilians", 1, [[
Dirija o seu bonde às pistas em San Fierro,
Deslize para abaixo de 5 km / h para parar na estação
e seja pago, não dirija rápido (64 km / h) ou
você pode descarrilar e ser demitido dentro de um segundo.

Pegue um bonde do próximo gerador ou use o seu próprio
em seguida, dirija sua rota.

Você receberá o pagamento em torno de R $ 130 / estação multiplicada
pelo seu nível atual (veja F5 para saber mais sobre
seu nivel). Você também ganhará R $ 15 / minuto para
cada passageiro que você pegar.

Armas:
- Desert eagle (4x)
- Baseball bat
	]], { -1, 60, 171, 172, 194, 253, 255 },
	{ "default", "Chinese casual dressed man", "White well dressed man", "White well dressed girl (blonde)", "White well dressed girl (brunette)", "Black man with uniform", "White man with mustache" },
	--[[WeaponID, amount, price, name]]
	{{5, 1, 40, "Baseball bat"}, {24, 28, 400, "Desert eagle"}},
	--[[Message for new players when applying for the job]]
	"Welcome to the tram driver job!, get a tram to start your work"},
	["Fireman"]={ "Emergency service", 0, [[
Aguarde que um fogo comece, (sempre que um veículo
explodir), então, vá o mais rápido possível para colocar
o fogo fora. Você pode usar a água do seu corpo de bombeiros
canon ou um extintor de incêndio (melhor pagamento).

Você receberá o pagamento de R $ 3000 / trabalho, (exige você
para trabalhar duro o tempo todo). Você pode fazer mais por
ajudando a polícia se você, por exemplo, bloquear a
suspeite com seu canon de água, etc.

Armas:
- Fire extingusher
	]], { 277, 278, 279 },
	{ "LSFD worker", "LVFD worker", "SFFD worker" },
	--[[WeaponID, amount, price, name]]
	{{42, 5000, 300, "Fire extinguesher"}},
	--[[Message for new players when applying for the job]]
	"Welcome to the fireman job!, look for any reported vehicle fire then go to that location"},
	["Paramedic"]={ "Emergency service", 0, [[
Encontre jogadores prejudicados e cure-os com o seu spray
pode, você também pode deixá-los entrar em sua ambulância para
Deixe-os hel, (exige que você esteja dentro).

Você receberá até R $ 500 / curar. Jogadores gratos
Pode dar-lhe uma recompensa extra para agradecer.

Armas:
- Spray can
	]], { 274, 275, 276 },
	{ "Black doctor", "Latino doctor", "White doctor" },
	--[[WeaponID, amount, price, name]]
	{{41, 500, 200, "Healing spray"}},
	--[[Message for new players when applying for the job]]
	"Welcome to the paramedic job!, heal players with your spray or take them to the hospital in your ambulance"},
	["Iron miner"]={ "Civilians", 3, [[
Desça a pedreira fora de Las Venturas e cave
Para minerais, as rochas que contêm minerasl são vistas por
um marcador semi-invisível, caminhe até ele e aguarde
escavando, então você obtém minerais. Venda seus minerais
na fábrica nerby quando seu bolso está cheio.

Você receberá o pagamento de R $ 4000 / caminhão totalmente carregado.

Armas:
- Desert eagle (x5)
- Baseball bat
	]], { -1, 27, 153, 260 },
	{ "default", "White iron miner", "The foreman", "Black iron miner" },
	--[[WeaponID, amount, price, name]]
	{{6, 1, 60, "Shovel"}, {24, 35, 500, "Desert eagle"}},
	--[[Message for new players when applying for the job]]
	"Welcome to the iron miner job!, press N to deploy dynamite, take cover and then mine by walking up to the rocks"},
	["Police officer"]={ "Government", 0, [[
Digite /wanted ou /fugitivos para ver quem é procurado, depois caça o
queria que as pessoas baixassem e prenda-os, o mais procurado
nivelar o pagamento mais, os suspeitos vivos valem mais
então evite matar a menos que seja necessário.

Guarda prisional
Uma missão lateral para o trabalho da polícia é tornar-se um
Guarda da prisão, para a prisão na marina de Bayside
e mata alguém tentando escapar ou alguém tentando
para ajudar alguém a escapar. Você é pago por morte.

Armas:
- Tazer (silenced) (x8)
- Teargas (x10)
- Nightstick
	]], { 280, 281, 282, 283, 288, 284 },
	{ "Los Santos police", "San Fierro police", "Las Venturas police", "Highway patrol (Bone county)", "Highway patrol (Red county)", "Traffic officer" },
	--[[WeaponID, amount, price, name]]
	{{3, 1, 0, "Nightstick"}, {23, 150, 200, "Tazer"}, {29, 30, 400, "MP5 (driveby)"}, {31, 50, 650, "M4 (heavy)"}, {17, 1, 100, "Teargas"}, {46, 1, 300, "Parachute"}},
	--[[Message for new players when applying for the job]]
	"Welcome to police job!, Hit wanted players with your nightstick to arrest"},
	["SAPD officer"]={ "Government", 0, [[
Digite /wanted ou /fugitivos para ver quem é procurado, depois caça o
queria que as pessoas baixassem e prenda-os, o mais procurado
nivelar o pagamento mais, os suspeitos vivos valem mais
então evite matar a menos que seja necessário.

Guarda prisional
Uma missão lateral para o trabalho da polícia é tornar-se um
Guarda da prisão, para a prisão na marina de Bayside
e mata alguém tentando escapar ou alguém tentando
para ajudar alguém a escapar. Você é pago por morte.

Armas:
- Tazer (silenced) (x8)
- Teargas (x10)
- Nightstick
	]], { 283, 288 },
	{ "Highway patrol (Bone county)", "Highway patrol (Red county)" },
	--[[WeaponID, amount, price, name]]
	{{3, 1, 0, "Nightstick"}, {23, 150, 200, "Tazer"}, {29, 30, 400, "MP5 (driveby)"}, {31, 50, 650, "M4 (heavy)"}, {17, 1, 100, "Teargas"}, {46, 1, 300, "Parachute"}},
	--[[Message for new players when applying for the job]]
	"Welcome to police job!, Hit wanted players with your nightstick to arrest"},
	["FBI agent"]={ "Government", 0, [[
Digite /wanted ou /fugitivos para ver quem é procurado, depois caça o
queria que as pessoas baixassem e prenda-os, o mais procurado
nivelar o pagamento mais, os suspeitos vivos valem mais
então evite matar a menos que seja necessário.

Guarda prisional
Uma missão lateral para o trabalho da polícia é tornar-se um
Guarda da prisão, para a prisão na marina de Bayside
e mata alguém tentando escapar ou alguém tentando
para ajudar alguém a escapar. Você é pago por morte.

Armas:
- Tazer (silenced) (x8)
- Teargas (x10)
- Nightstick
	]], { 165, 166, 286, 295 },
	{ "Agent Jay", "Agent Kay", "FBI officer", "Mike" },
	--[[WeaponID, amount, price, name]]
	{{3, 1, 0, "Nightstick"}, {23, 150, 200, "Tazer"}, {29, 30, 400, "MP5 (driveby)"}, {31, 50, 650, "M4 (heavy)"}, {17, 1, 100, "Teargas"}, {46, 1, 300, "Parachute"}},
	--[[Message for new players when applying for the job]]
	"Welcome to police job!, Hit wanted players with your nightstick to arrest"},
	["SWAT officer"]={ "Government", 0, [[
Digite /wanted ou /fugitivos para ver quem é procurado, depois caça o
queria que as pessoas baixassem e prenda-os, o mais procurado
nivelar o pagamento mais, os suspeitos vivos valem mais
então evite matar a menos que seja necessário.

Guarda prisional
Uma missão lateral para o trabalho da polícia é tornar-se um
Guarda da prisão, para a prisão na marina de Bayside
e mata alguém tentando escapar ou alguém tentando
para ajudar alguém a escapar. Você é pago por morte.

Armas:
- Tazer (silenced) (x8)
- Teargas (x10)
- Nightstick
	]], { 285 },
	{ "SWAT officer" },
	--[[WeaponID, amount, price, name]]
	{{3, 1, 0, "Nightstick"}, {23, 150, 200, "Tazer"}, {29, 30, 400, "MP5 (driveby)"}, {31, 50, 650, "M4 (heavy)"}, {17, 1, 100, "Teargas"}, {46, 1, 300, "Parachute"}},
	--[[Message for new players when applying for the job]]
	"Welcome to police job!, Hit wanted players with your nightstick to arrest"},
	["Armed forces"]={ "Government", 0, [[
Digite /wanted ou /fugitivos para ver quem é procurado, depois caça o
queria que as pessoas baixassem e prenda-os, o mais procurado
nivelar o pagamento mais, os suspeitos vivos valem mais
então evite matar a menos que seja necessário.

Guarda prisional
Uma missão lateral para o trabalho da polícia é tornar-se um
Guarda da prisão, para a prisão na marina de Bayside
e mata alguém tentando escapar ou alguém tentando
para ajudar alguém a escapar. Você é pago por morte.

Armas:
- Tazer (silenced) (x8)
- Teargas (x10)
- Nightstick
	]], { 287 },
	{ "Soldier" },
	--[[WeaponID, amount, price, name]]
	{{3, 1, 0, "Nightstick"}, {23, 150, 200, "Tazer"}, {29, 30, 400, "MP5 (driveby)"}, {31, 50, 650, "M4 (heavy)"}, {17, 1, 100, "Teargas"}, {46, 1, 300, "Parachute"}},
	--[[Message for new players when applying for the job]]
	"Welcome to police job!, Hit wanted players with your nightstick to arrest"}
}

-- Restrict jobs to specific groups
restricted_jobs = {
	["FBI agent"]="FBI",
	["SWAT officer"]="SWAT",
	["SAPD officer"]="SAPD",
	["Armed forces"]="ArmedForces"
}

markers = { }
