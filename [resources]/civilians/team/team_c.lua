

local refreshTimer = nil
x,y = guiGetScreenSize()
window = exports.UI:createWindow((x-400)/2, (y-300)/2, 400, 300, ".:: Carteira de Trabalho ::.", false )
closeButton = guiCreateButton(280,260,110,36,"Fechar",false,window)
endWorkButton = guiCreateButton(10,260,110,36,"Abandonar Trabalho",false,window)
crimWorkButton = guiCreateButton(124,260,110,36,"Tornar-se Criminoso",false,window)
jobRankLabel = guiCreateLabel( 20, 70, 100, 70, "", false, window )
jobValueLabel = guiCreateLabel( 120, 70, 200, 70, "", false, window )
exports.UI:setDefaultFont(closeButton, 10)
exports.UI:setDefaultFont(endWorkButton, 10)
exports.UI:setDefaultFont(crimWorkButton, 10)
exports.UI:setDefaultFont(jobRankLabel, 10)
exports.UI:setDefaultFont(jobValueLabel, 10)
guiSetVisible(window,false)
addEventHandler("onClientGUIClick",root,function()
	if source == closeButton then
		guiSetVisible(window, false)
		exports.UI:showGUICursor(false)
	elseif source == endWorkButton then
		if getPlayerWantedLevel() > 0 then exports.topbar:dm("Você não pode terminar seu trabalho devido ao seu nível de procurado!", 255, 0, 0) return end
		triggerServerEvent( "civilians_onEndWork", localPlayer, localPlayer )
		triggerEvent( "civilians_onEndWork", localPlayer, localPlayer )
		refreshTimer = setTimer(refreshRank, 500, 20)
	elseif source == crimWorkButton then
		triggerServerEvent( "civilians_gocrim", localPlayer, localPlayer )
		refreshTimer = setTimer(refreshRank, 500, 20)
	end
end)

-- Asyncron event to receive a player rank client side
addEvent ( "onReceivePlayerRank", true )
function receiveRankEventHandler( rank, next, currentValue, nextValue, levelID )
	guiSetText( jobRankLabel, "Carreira : \nNivel Atual : \nProximo Nivel : \nProgresso : " )
	local occ = getElementData(localPlayer,"Occupation")
	if not occ or occ == "" then
		occ = "Unemployed"
	end
	guiSetText( jobValueLabel, occ.."\n"..rank.." (Nivel: "..levelID..")".."\n"..next.."\n"..currentValue.." of "..nextValue )
end
addEventHandler ( "onReceivePlayerRank", localPlayer, receiveRankEventHandler )

function refreshRank()
	if not guiGetVisible(window) then return end
	if isTimer(refreshTimer) then killTimer(refreshTimer) end
	get_player_rank(getElementData(localPlayer,"Occupation"))
end

function showWorkGUI( )
	guiSetVisible(window, not guiGetVisible(window))
	exports.UI:showGUICursor(not isCursorShowing())
	get_player_rank(getElementData(localPlayer,"Occupation"))
end
addCommandHandler( "managework", showWorkGUI )
addCommandHandler( "trabalho", showWorkGUI )
addCommandHandler( "job", showWorkGUI )
bindKey( "F5", "down", "managework" )
