--[[ Get the rank for given occupation ]]--
function get_player_rank(occupation)
	triggerServerEvent("onAskForPlayerRank", localPlayer, getElementData(localPlayer, "Occupation"))
end

--[[ Response from server are handled here ]]--
function receive_rank(rank, nextRank, curr, next, statLevel)
    triggerEvent( "onReceivePlayerRank", localPlayer, rank, nextRank, curr, next, statLevel )
end
addEvent("onRankReceive", true)
addEventHandler("onRankReceive", localPlayer, receive_rank)
