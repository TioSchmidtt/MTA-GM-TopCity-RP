
-- Global client properites
p_account = nil
p_loggedIn = false
sec_login_attempts = 0
login_cooldown = nil

-- Display status messages from the server
addEvent("accounts:onStatusReceive", true)
addEventHandler("accounts:onStatusReceive", root, function(msg, statusCode)
	if statusCode == -1 then
		guiLabelSetColor(labelInfo, 255, 0, 0)
	elseif statusCode == 0 then
		guiLabelSetColor(labelInfo, 255, 255, 0)
	elseif statusCode == 1 then
		guiLabelSetColor(labelInfo, 0, 255, 0)
	end
	guiSetText(labelInfo, msg)
end)

-- Setup the login GUI (UI must be running)
function make_login()
	x,y = guiGetScreenSize()
	window = exports.UI:createWindow((x-600)/2, (y-450)/2, 600, 450, "TopCity-RP v4.2", false )
	loginButton = guiCreateButton(480,400,110,40,"Login",false,window)
	registerButton = guiCreateButton(368,400,110,40,"Registrar",false,window)
	introButton = guiCreateButton(256,400,110,40,"Ver intro",false,window)
	updatesButton = guiCreateButton(10,400,116,40,"Updates",false,window)
	labelUser = guiCreateLabel(30, 40, 290, 25, "Usuário:", false, window)
	labelPwrd = guiCreateLabel(30, 100, 290, 25, "Senha:", false, window)
	textUser = guiCreateEdit(30,65,290,25,"",false,window)
	textPwrd = guiCreateEdit(30,125,290,25,"",false,window)
	labelInfo = guiCreateLabel(30, 210, 290, 80, "", false, window)
	checkBoxUser = guiCreateCheckBox(30, 160, 290, 20, "Salvar Usuário", false, false, window)
	checkBoxPwrd = guiCreateCheckBox(30, 180, 290, 20, "Salvar Senha", false, false, window)
	-- Shaders control panel
	labelShaders = guiCreateLabel(360, 40, 230, 25, "Shader Gráficos:", false, window)
	checkBoxDetail = guiCreateCheckBox(360, 80, 290, 20, "Texturas Detalhadas (Pesado)", false, false, window)
	checkBoxContrast = guiCreateCheckBox(360, 100, 290, 20, "Contrast (Pesado)", false, false, window)
	checkBoxWater = guiCreateCheckBox(360, 120, 290, 20, "Água (Medio) ", false, false, window)
	checkBoxCarPaint = guiCreateCheckBox(360, 140, 290, 20, "Pintura Veiculos (Leve)", false, false, window)
	labelInfoRight = guiCreateLabel(360, 180, 290, 170,
[[Welcome! para se inscrever, escolha
 a conta
nome e senha, em seguida, clique em
registrar.
Para fazer login, insira as credenciais
e clique em
entrar. Você também pode selecionar
shaders para
fazer o jogo parecer mais realista ou
desabilite shaders para melhor FPS.

Saudações: eXemple Gaming
Blog: topcity-rp.blogspot.com.br]], false, window)
	guiEditSetMasked(textPwrd, true)
	guiSetInputEnabled(true)

	-- Friends invite bonus
	labelFacc = guiCreateLabel(30, 300, 190, 25, "Envie um Gift a seu amigo", false, window)
	textFacc = guiCreateEdit(30,325,260,25,"",false,window)
	faccButtonHelp = guiCreateButton(290,325,30,25,"?",false,window)

	-- Set GUI font
	exports.UI:setDefaultFont(loginButton, 10)
	exports.UI:setDefaultFont(registerButton, 10)
	exports.UI:setDefaultFont(introButton, 10)
	exports.UI:setDefaultFont(updatesButton, 10)
	exports.UI:setDefaultFont(labelUser, 10)
	exports.UI:setDefaultFont(labelPwrd, 10)
	exports.UI:setDefaultFont(labelInfo, 10)
	exports.UI:setDefaultFont(textUser, 9)
	exports.UI:setDefaultFont(textPwrd, 9)
	exports.UI:setDefaultFont(checkBoxUser,10)
	exports.UI:setDefaultFont(checkBoxPwrd, 10)
	exports.UI:setDefaultFont(labelFacc, 10)
	exports.UI:setDefaultFont(textFacc, 10)
	exports.UI:setDefaultFont(faccButtonHelp, 10)
	exports.UI:setDefaultFont(checkBoxDetail, 10)
	exports.UI:setDefaultFont(checkBoxContrast, 10)
	exports.UI:setDefaultFont(checkBoxCarPaint, 10)
	exports.UI:setDefaultFont(checkBoxWater, 10)
	exports.UI:setDefaultFont(labelInfoRight, 10)
	exports.UI:showGUICursor(true)

	-- Load login details from xml
	local f = xmlLoadFile('@data.xml', 'account')
	if f then
		local user = xmlNodeGetAttribute(xmlFindChild(f, 'user', 0), 'value') or ""
		local pass = xmlNodeGetAttribute(xmlFindChild(f, 'pass', 0), 'value') or ""
		local sh_detail = xmlNodeGetAttribute(xmlFindChild(f, 'detail', 0), 'value') or ""
		local sh_contrast = xmlNodeGetAttribute(xmlFindChild(f, 'contrast', 0), 'value') or ""
		local sh_water = xmlNodeGetAttribute(xmlFindChild(f, 'water', 0), 'value') or ""
		local sh_carpaint = xmlNodeGetAttribute(xmlFindChild(f, 'carpaint', 0), 'value') or ""
		guiSetText(textUser, tostring(user))
		guiSetText(textPwrd, tostring(pass))
		if user ~= "" then
			guiCheckBoxSetSelected(checkBoxUser, true)
		end
		if pass ~= "" then
			guiCheckBoxSetSelected(checkBoxPwrd, true)
		end
		if sh_detail == "enabled" then
			guiCheckBoxSetSelected(checkBoxDetail, true)
			exports.shader_detail:toggleShaderDetail(true)
		else
			exports.shader_detail:toggleShaderDetail(false)
		end
		if sh_contrast == "enabled" then
			guiCheckBoxSetSelected(checkBoxContrast, true)
			exports.shader_contrast:toggleShaderContrast(true)
		else
			exports.shader_contrast:toggleShaderContrast(false)
		end
		if sh_water == "enabled" then
			guiCheckBoxSetSelected(checkBoxWater, true)
			exports.shader_water:toggleShaderWater(true)
		else
			exports.shader_water:toggleShaderWater(false)
		end
		if sh_carpaint == "enabled" then
			guiCheckBoxSetSelected(checkBoxCarPaint, true)
			exports.shader_car_paint_reflect:toggleShaderCarPaint(true)
		else
			exports.shader_car_paint_reflect:toggleShaderCarPaint(false)
		end
	end
end

-- Login GUI click events
addEventHandler("onClientGUIClick",root,function()
	-- On login (asyncron function)
	if source == loginButton then
		if isTimer(login_cooldown) then return end
		guiLabelSetColor(labelInfo, 255, 255, 255)
		guiSetText(labelInfo, "Tentando Logar ... por favor aguarde")
		fadeCamera(false, 1)
		setTimer(triggerServerEvent, 1100, 1, "accounts:attemptClientLogin", localPlayer, guiGetText(textUser), guiGetText(textPwrd))
		if sec_login_attempts > 2 then
			-- Kick after 3 failed login attempts
			triggerServerEvent("accounts:kickClientSpammer", localPlayer)
		end
		sec_login_attempts = sec_login_attempts + 1
		login_cooldown = setTimer(function()
			toggle_gui_enable(true)
		end, 3000, 1)
		toggle_gui_enable(false)
	-- On registration
	elseif source == registerButton then
		triggerServerEvent("accounts:onClientAttemptRegistration", localPlayer, guiGetText(textUser), guiGetText(textPwrd), guiGetText(textFacc))
	-- On view intro start
	elseif source == introButton then
		addEventHandler("onClientRender", root, view_gtw_intro)
		guiSetVisible(window, false)
	-- On view updates button click (requires GTWupdates)
	elseif source == updatesButton then
		exports.GTWupdates:viewUpdateListGUI()
	-- On help request click
	elseif source == faccButtonHelp then
		guiSetText(labelInfo, "Digite o nome da sua conta de amigos \n não envie-lhe um bônus de convidado \n R$4,000 como recompensa.")
		guiLabelSetColor(labelInfo, 255, 255, 255)
	-- Toggle shaders
	elseif source == checkBoxDetail then
		exports.shader_detail:toggleShaderDetailCMD()
	elseif source == checkBoxContrast then
		exports.shader_contrast:toggleShaderContrastCMD()
	elseif source == checkBoxWater then
		exports.shader_water:toggleShaderWaterCMD()
	elseif source == checkBoxCarPaint then
		exports.shader_car_paint_reflect:toggleShaderCarPaintCMD()
	end
end)

-- Enable/disable buttons
function toggle_gui_enable(state)
	guiSetEnabled(loginButton, state)
	guiSetEnabled(registerButton, state)
	guiSetEnabled(updatesButton, state)
	guiSetEnabled(faccButtonHelp, state)
	guiSetEnabled(textUser, state)
	guiSetEnabled(textPwrd, state)
	guiSetEnabled(textFacc, state)
end

-- On client attempt login
addEvent("accounts:onClientPlayerLogin", true)
addEventHandler("accounts:onClientPlayerLogin", root, function(acnt)
	local f = xmlCreateFile("@data.xml", "account")
	local user, pass = "", ""
	if guiCheckBoxGetSelected(checkBoxUser) then
		user = guiGetText(textUser)
	end
	if guiCheckBoxGetSelected(checkBoxPwrd) then
		pass = guiGetText(textPwrd)
	end
	xmlNodeSetAttribute(xmlCreateChild(f, "user"), "value", user)
	xmlNodeSetAttribute(xmlCreateChild(f, "pass"), "value", pass)
	-- Save shaders
	if guiCheckBoxGetSelected(checkBoxDetail) then
		xmlNodeSetAttribute(xmlCreateChild(f, "detail"), "value", "enabled")
	else
		xmlNodeSetAttribute(xmlCreateChild(f, "detail"), "value", "disabled")
	end
	if guiCheckBoxGetSelected(checkBoxContrast) then
		xmlNodeSetAttribute(xmlCreateChild(f, "contrast"), "value", "enabled")
	else
		xmlNodeSetAttribute(xmlCreateChild(f, "contrast"), "value", "disabled")
	end
	if guiCheckBoxGetSelected(checkBoxWater) then
		xmlNodeSetAttribute(xmlCreateChild(f, "water"), "value", "enabled")
	else
		xmlNodeSetAttribute(xmlCreateChild(f, "water"), "value", "disabled")
	end
	if guiCheckBoxGetSelected(checkBoxCarPaint) then
		xmlNodeSetAttribute(xmlCreateChild(f, "carpaint"), "value", "enabled")
	else
		xmlNodeSetAttribute(xmlCreateChild(f, "carpaint"), "value", "disabled")
	end
	xmlSaveFile(f)
	xmlUnloadFile(f)
	guiSetVisible(window, false)
	exports.UI:showGUICursor(false)
	guiSetInputEnabled(false)
	showChat(true)

	-- Set account obtained from the server and login status
	p_loggedIn = true
	p_account = acnt
end)

-- Exported functions to check if logged in or to obtain account name
function getPlayerAccount()
	return p_account or "Guest"
end
function isClientLoggedIn()
	return p_loggedIn
end

-- Get a players ID
function getPlayerId(p)
	local id = nil
	for i, v in pairs(getElementsByType('player')) do
		if v == p then
			id = i
			break
		end
	end
	return id
end

-- Display login screen to players who isn't currently logged in
addEventHandler("onClientResourceStart", resourceRoot, function()
	if not getElementData(localPlayer, "isLoggedIn") then
		exports.topbar:dm("Welcome! Leia as instruções antes de tentar registrar", 0,200,0)
		setTimer(make_login, 500, 1)
		setBlurLevel(0)
		showChat(false)
		triggerServerEvent("accounts.onClientSend",localPlayer)
	else
		p_loggedIn = true
	end
end)
