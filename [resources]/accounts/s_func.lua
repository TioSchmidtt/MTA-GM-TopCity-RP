
-- Attempts cache
login_cache = {}

--[[ On client login request attempt ]]--
function client_login_attempt(user, pass)
	-- Player is already logged in
	if not getPlayerAccount(client) or not isGuestAccount(getPlayerAccount(client)) then
		triggerClientEvent(source, "accounts:onClientPlayerLogin", client, getAccountName(getPlayerAccount(client)))
		return
	end

	-- Get any account with the provided name "user"
	local acc = getAccount(user)

	-- Does the account exist?
	if not acc then
		return display_status("O nome da conta não foi encontrado Pressione 'Registrar'\n"..
			"para criar uma nova conta com o atual\n"..
			"forneceu credenciais", client, 0)
	end

	-- Yes it does exist, let's try to login with the given password "pass"
	if not logIn(client, acc, pass) then
		return display_status("Senha incorreta para esta conta.", client, -1)
	end

	-- Tell the client that the login was successfull
	triggerClientEvent(source, "accounts:onClientPlayerLogin", client, user)
end
addEvent("accounts:attemptClientLogin", true)
addEventHandler("accounts:attemptClientLogin", root, client_login_attempt)

--[[ Kick after 3 failed login attempts ]]--
function kick_after_three_fails()
	-- Inform the player about how to signup
	exports.topbar:dm("Para registrar uma nova conta: insira credenciais nos campos e clique em 'Registrar'", client, 255, 255, 255)

	-- Ignore for now, a client cooldown timer prevent spam of login
	--kickPlayer(client, "Calm down and read the status message before going insane!")
end
addEvent("accounts:kickClientSpammer", true)
addEventHandler("accounts:kickClientSpammer", root, kick_after_three_fails)

--[[ On client registration attempt ]]--
function client_registration_attempt(user, pass, facc)
	-- Check if any username or password where provided at all
	if not user or not pass then
		display_status("Nome de usuário e senha esperados!", client, 0)
		return
	end

	-- Check how many account that has been registred from this PC
	local serial = getPlayerSerial(client)
	local accounts = getAccountsBySerial(serial)
	if #accounts > 9 then
		return display_status("Muitas contas já foram registradas"..
			"\nNeste PC! Entre em contato com um administrador para obter mais assistência.", client, -1)
	end

	-- Save current
	local old_usr = user

	-- Validate input
	local char = "[^0-9a-zA-Z_]"
	user = string.gsub(user, char, "")

	-- This account name has already been used (converted names are validated too)
	local acn = getAccount(user)
	if acn then
		return display_status("O nome da conta já está em uso,\ntente outro.", client, -1)
	end

	-- Invalid symbold where used, tell the user how a proper account name are written
	if old_usr ~= user then
		return display_status("Nome de usuário inválido, você só pode usar \ntele segue os símbolos: 0-9, a-z, A-Z or _", client, -1)
	end

	-- Check if there's a friend provided
	local friend = getAccount(facc)
	acn = addAccount(user, pass)

	-- Shit, something went wrong, unable to add the account, check the
	-- servers error log for further information
	if not acn then
		display_status("Falha ao adicionar a conta.\nPode existir uma conta com outra\nvariação do caso do nome que você forneceu!", client, -1)
		return
	end

	-- Alright we're in, now let's see if this guy has a friend or not
	display_status("Você se registrou! \nAgora você pode fazer o login.", source, 1)
end
addEvent("accounts:onClientAttemptRegistration", true)
addEventHandler("accounts:onClientAttemptRegistration", root, client_registration_attempt)

--[[ Didn't knew about this feature? don't worry, you can still give your friend his invite bonus ]]--
function send_invite_bonus(thePlayer, cmd, facc)
	-- Is there a friend provided?
	local friend = getAccount(facc)
	if not friend or not facc then
		outputChatBox("Correct syntax: /sendinvitebonus <friends_account_name>", thePlayer, 255, 255, 255)
		return
	end
end
addCommandHandler("sendinvitebonus", send_invite_bonus)

-- Send status message to clients, code -1 = red, 0 = yellow, 1 = green
function display_status(msg, p, statusCode)
	return triggerClientEvent(p, "accounts:onStatusReceive", p, msg, statusCode)
end

-- Set player unique ID on login
addEventHandler("onPlayerJoin", resourceRoot,
	function ()
		setElementData(source, "ID", tostring(getPlayerId(source)))
	end
)

-- Give all players their unique ID once resource is restarted
addEventHandler("onResourceStart", resourceRoot,
	function ()
		for i, v in pairs(getElementsByType("player")) do
			setElementData(v, "ID", tostring(i))
		end
	end
)

-- Helper function to obtain a players IP
function getPlayerId(p)
	for i, v in pairs(getElementsByType("player")) do
		if p == v then
			return i
		end
	end
end

-- Fade camera and set the player as target on login
addEventHandler("onPlayerLogin", root,
	function (_, acc)
		fadeCamera(source, true, 5)
		setCameraTarget(source, source)

		-- Get position
		local posX = exports.core:get_account_data(acc, "GTWdata.loc.x")
		local posY = exports.core:get_account_data(acc, "GTWdata.loc.y")
		local posZ = exports.core:get_account_data(acc, "GTWdata.loc.z")
		local rotZ = exports.core:get_account_data(acc, "GTWdata.loc.rot.z")

		-- Set a value indicating this is the first spawn to
	        -- ensure player data is loaded as soon the player spawns
	        setElementData(source, "GTWdata.isFirstSpawn", true)

		-- Get player location x y z
	    	if (posX and posY and posZ) then
		 	spawnPlayer(source, posX, posY, posZ, rotZ, getElementModel(source), getElementInterior(source), getElementDimension(source), getPlayerTeam(source))
			setCameraTarget(source, source)

			-- Temporary (from 2015-07-01) restore health and armor
			--local health = exports.core:get_account_data(playeraccount, "GTWdata.health")
			--local armor = exports.core:get_account_data(playeraccount, "GTWdata.armor")
	        	--setPedArmor(source, armor)
			--setElementHealth(source, health)
		else
			local x,y,z,r = unpack(spawn_loc[math.random(#spawn_loc)])
			spawnPlayer(source, x, y, z+3, r, skin_id[math.random(#skin_id)], 0, 0, getTeamFromName("Unemployed"))
			setElementFrozen(source, true)
			setTimer(setElementFrozen, 1000, 1, source, false)
	    	end
	end
)

--[[ Show downloading information ]]--
addEventHandler("onPlayerJoin", root,
	function ()
		s_display[source] = {}
		s_text[source] = {}

		s_text[source][1] = textCreateTextItem("'Paciência , Não faça nada de estupido'", 0.5, 0.5,1,200,200,200,255,2.2,"center","center",200)
		s_text[source][2] = textCreateTextItem("# Leia as regras/intruções no F1 Assim que conectar #", 0.5, 0.91,1,200,200,200,255,1.4,"center","center",200)
		s_text[source][3] = textCreateTextItem("Aguarde enquanto estamos baixando o TopCity-RP v4.2 game mode...", 0.5, 0.1,1,200,200,200,255,1.4,"center","center",200)

		for w=1, #s_text[source] do
		   	s_display[source][w] = textCreateDisplay()
			textDisplayAddObserver(s_display[source][w], source)
			textDisplayAddText(s_display[source][w], s_text[source][w])
		end
	end
)

addCommandHandler("gtainfo", function(plr, cmd)
	outputChatBox("[TopCity-RP] "..getResourceName(
	getThisResource())..", by: "..getResourceInfo(
        getThisResource(), "author")..", v-"..getResourceInfo(
        getThisResource(), "version")..", is represented", plr)
end)

addEvent("accounts.onClientSend",true)
addEventHandler("accounts.onClientSend", root,
	function ()
		if isGuestAccount(getPlayerAccount(client)) then
			-- Clear text from screen
			if s_display[client] then
				for k=1, #s_display[client] do
					textDisplayRemoveObserver(s_display[client][k], client)
				   	s_display[client][k] = nil
				   	s_text[client][k] = nil
				end
			end

			-- Setup a default view
			local x,y,z,x2,y2,z2 = unpack(s_views[math.random(#s_views)])
			setCameraMatrix( client, x,y,z, x2,y2,z2, 2 )
			fadeCamera(client, true, 1)

			-- Save view coordinates
			setElementData(client, "accounts.login.coordinates.x", x)
			setElementData(client, "accounts.login.coordinates.y", y)
			setElementData(client, "accounts.login.coordinates.z", z)
			setElementData(client, "accounts.login.coordinates.x2", x2)
			setElementData(client, "accounts.login.coordinates.y2", y2)
			setElementData(client, "accounts.login.coordinates.z2", z2)

			-- Ability for clients to see if a player is logged in or not
			setElementData(client, "isLoggedIn", true)
		end
	end
)
