
local e_time = getRealTime()

-- 2015-11-14 to 21
setTimer(function()
        if e_time.year == 115 and e_time.month == 11 and
                (e_time.monthday > 13 or e_time.monthday < 21) then
                for k,v in pairs(getElementsByType("player")) do
                        local x,y,z = getElementPosition(v)
                        if (getElementData(v, "core.event.px") or 0) ~= x and
                                (getElementData(v, "core.event.py") or 0) ~= y then
                                        givePlayerMoney(v, 1000)
                                        setElementData(v, "core.event.px", x)
                                        setElementData(v, "core.event.py", y)
                                        exports.topbar:dm("EVENTO: Você recebeu R$ 1000 "..
                                        "por estar online hoje!", v,255,255,255)
                        end
                end
        end
end, 5*60*1000, 0)
