

--[[ Countdown timer manager ]]--
countdownTimers = {}
function showCountDown(plr, cmd, seconds, ...)
	local text = table.concat({ ... }, " ")
	local x,y,z = getElementPosition(plr)
	if seconds and text and math.floor(seconds) < 20 and math.floor(seconds) > 2 then
		seconds = math.floor(seconds)
    	for k,v in pairs(getElementsByType("player")) do
        	local rx,ry,rz = getElementPosition(v)
        	if getDistanceBetweenPoints3D(x,y,z, rx,ry,rz) < 100 and not isTimer(countdownTimers[v]) then
        		outputChatBox("-- "..getPlayerName(plr).." solicitou uma contagem regressiva -", v, 255, 255, 255)
        		countdownTimers[v] = setTimer(cd_timer_count, 1000, seconds, v, seconds)
        		setTimer(outputChatBox, 1000*seconds+1000, 1, text, v, 255, 255, 255)
        	elseif isTimer(countdownTimers[v]) then
        		exports.topbar:dm("Um cronômetro já está contando nesta área, aguarde!", plr, 255, 0, 0)
        	end
    	end
	else
		outputChatBox("Correct syntax: /contar <seconds> <text>", plr, 255, 255, 255)
	end
end
addCommandHandler("countdown", showCountDown)
addCommandHandler("Contagemregressiva", showCountDown)
addCommandHandler("contar", showCountDown)
addCommandHandler("timer", showCountDown)

-- Display a count down timer for race or similar
function cd_timer_count(owner, timeInSeconds)
	local i1,i2,i3 = getTimerDetails(countdownTimers[owner])
    	outputChatBox("#66FF00[Contagem regressiva] #FFFFFF"..i2, owner, 255, 255, 255, true)
end
