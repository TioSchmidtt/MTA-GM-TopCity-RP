--[[
***********************************************************************
	Project owner:		RageQuit community
	Project name: 		TopCity-RP
	Developers:   		Mr_Moose

	Source code:		https://github.com/404rq/TopCity-RP/
	Bugtracker: 		https://discuss.404rq.com/t/issues
	Suggestions:		https://discuss.404rq.com/t/development

	Version:    		Open source
	License:    		BSD 2-Clause
	Status:     		Stable release
***********************************************************************
]]--

--[[ All help related text is in this file ]]--
list = {
	{ "Rules", 0 },
	{ "English", 1, [[
***********************************************************************
* Rules in English
***********************************************************************

#1 Do not spawn kill other players.
#2 Drive on the right side of the road. Do not block other players, do not crash into other players unless you have a reason to do so, e.g in a police chase or to revenge.
#3 Do not use any kind of hack or exploit bugs. TopCity-RP is an open source game mode which we claim to be stable and of high quality, it's therefore important to let us know about any bug you may find. Bug reports are rewardable, exploits are punishable.
#4 Do not advertise, feel free to talk about anything as long you're not advertising it. If you don't get the difference here's an example: Talking: xBox vs PS3... Advertising: buy xBox here...
#5 Do not obstruct or interrupt role play situations. It's not appropriate to show up with a minigun and kill a group of players standing together chatting for instance, do not use more violence than the situation require.
#6 Do not spam the chats, repeating nonsense on purpose will lead to a mute, note that this mute are global and applies to many other servers running TopCity-RP resources as well.
#7 Do NOT try this in game: apply for staff, suggest new features, complaint on other players or staff, ask about stuff you'll find information about in the list to the left. This is not punishable but you may not get the response you expected.
#8 Do not actively troll, provoke, annoy or insult other players. Instead, solve your conflicts here: Report/Appeal: https://forum.404rq.com/complaints-and-reports/

Thank you for complying with our rules.

***********************************************************************
* Last edit: 2015-01-28
***********************************************************************]] },
	{ "Portuguese (Portuguese)", 1, [[
***********************************************************************
* Rules in Portuguese (Portuguese)
***********************************************************************
#1 Não faça spawn kill em outros jogadores. (Aplica-se ao redor de hospitais)
#2 Dirigir sempre no lado direito da estrada. Não bloquear os outros jogadores, não bater em outros jogadores, a menos que você tenha uma razão para fazer isso, por exemplo, em uma perseguição policial ou por vingança.
#3 Não use qualquer tipo de hack ou exploração de bugs. TopCity-RP é um modo de jogo de código aberto que pretendemos deixar estável e de alta qualidade, Portanto, é importante que você nos informe sobre qualquer bug, que você encontrar. Relatórios de bug são recompensados, explarações são puníveis..
#4 Não anunciar, sinta-se livre para falar sobre qualquer coisa, contanto que você não anuncie. Se você não entende a diferença aqui está um exemplo: Falando: xBox vs PS3... Publicidade: compre xBox aqui...
#5 Não obstrua ou interrompa as situações de role play. Não é apropriado aparecer com uma minigun e matar um grupo de jogadores que estão juntos conversando, por exemplo. Não use mais violência do que a situação requer.
#6 Não faça spam no chat, se repetir propositalmente você sera mutado, note que o mute é global e aplicado em todos os servidores GTW.
#7 Não tente isso em jogo: pedir para ser staff, sugerir novas funcionalidades, reclamar sobre outros jogadores ou staff, Perguntar sobre coisas que estão no menu à esquerda. Isso não é punível, mas você pode não obter a resposta que você esperava.
#8 Não seja um troll, não provoque, irrite ou insulte os outros jogadores. Em vez disso, resolve seus conflitos aqui: https://forum.404rq.com/complaints-and-reports/

Obrigado por cumprir com as nossas regras.


***********************************************************************
* Last edit: 2015-05-27
***********************************************************************]] },
	{ "Geral", 0 },
	{ "Comandos de contas de usuário", 1, [[
***********************************************************************
* Sobre as contas de usuário
***********************************************************************

Como fazer login ou registrar:
  * Digite suas credenciais e, em seguida, pressione login e seus dados serão salvos e sincronizados
  * Se você não possui nenhuma conta, escolha e insira todas as credenciais e, em seguida, pressione "registrar", com sucesso você verá um texto verde, pressione login para entrar na sua nova conta.

Como alterar a senha:
  * Pressione F8 para abrir o console e, em seguida, digite:

  chgmypass <senha_antiga> <novas_senha>

Como mudar o apelido/nick:
  * Pressione F8 para abrir o console e, em seguida, digite:

  nick <new-nick>

Observe que esses comandos são criados e funcionam em qualquer servidor Multi Theft Auto.
]]},
	{ "Diretrizes da Staff", 1, [[
***********************************************************************
* Diretrizes de pessoal (apenas relevantes para staff do servidor)
***********************************************************************

Como faço para entrar no "modo pessoal":
  * Use /gostaff(f/m) é opcional e especifica se você deseja usar uma skin masculina ou feminina


Onde está o painel de admin:
  * Use /staff ou tecla 'P'

Como gerar um veículo:
  * Use /gv para abrir o menu de criação de aluguel em qualquer lugar do mapa, todos os veículos são gratuitos

Como Usar teleport:
  * Use /setpos x y z ou abra o painel para a mais opções

Preciso usar tags:
  * Não, é completamente opcional, independentemente do modo em que você estiver
]]},
	{ "Como evitar o queda de FPS", 1, [[
***********************************************************************
* Como evitar o queda de FPS
***********************************************************************

Você pode alternar os shader para melhorar seu FPS

Alterar a reflexão da pintura do carro shader:
  /reflect

Alterar shader de detalhes altos:
  /detail

Alterar shader de água (se você conseguir gotas FPS perto de lagos ou mar):
  /water

Alterar alto contraste shader:
  /contrast

Você pode alternar localmente o clima para aumentar seu FPS também, usando os seguintes comandos:

/sunny - ensolarado
/rainy - chuvoso
/foggy - nevoento

]]},
	{ "Como evitar alta latência (PING)", 1, [[
***********************************************************************
* Como evitar alta latência
***********************************************************************

A alta latência ocorre por dois motivos, ou a rede está sobrecarregada ou você está muito longe do servidor, considere, por exemplo, se você está conectado a uma rede sem fio com lote de distúrbios (o que praticamente todas as redes sem fio tem mais ou menos ).

Para evitar alta latência, você não deve considerar essas duas coisas:
   * Se possível, use uma rede de cabo em vez de uma rede sem fio. Isso deve reduzir sua latência para o máximo de 400, independentemente de onde você estiver localizado no mundo (valores mais baixos se aplicam à Europa e aos EUA, uma vez que é a nossa localização).
   * Evite baixar arquivos durante a reprodução, especialmente arquivos torrent e semelhantes que tendem a transbordar redes locais, também certifique-se de que ninguém mais na mesma rede não está baixando nada em segundo plano.

O que acontece quando a latência é muito alta?
Bloqueamos alguns recursos para você temporariamente, na pior das hipóteses, nós o expulsamos. Estes sistemas são automáticos, por isso não culpe pessoal online por sua alta latência.
]]},
	{ "F.A.Q", 1, [[
***********************************************************************
* Perguntas frequentes
***********************************************************************

P: Como eu mudo minha skin?
R: Procure uma camiseta no seu radar, entre na loja, caminhe até o marcador e selecione uma nova skin da loja

P: Como faço dinheiro?
R: Procure um trabalho, (vá até um marcador amarelo para abrir a GUI do aplicativo), faça coisas 'honestas' como roubar lojas, roubar carros, matar bots, etc.

Q: Onde posso encontrar um carro?
R: Você pode alugar um carro em qualquer marcador branco perto de postos de gasolina, você também pode comprar um veículo da Concessionária de automóveis são marcadores com um nome de tipo de veículo que vende naquela loja. 

P: Como posso me tornar staff?
R: Visite: https://topcity-rp.blogspot.com.br/staff para aprender tudo o que você precisa saber

P: Existe alguma outra maneira?
R: alguns, embora nenhum deles funcione, se você incomodar outros jogadores/staff ou por falta de células cerebrais

P: Posso me tornar staff convidando os jogadores?
R: Não, mas você pode ganhar um bom dinheiro por convites se você instruí-los a inserir o nome da sua conta no campo do convite

P: Existe algum Blog?
A: Sim: https://topcity-rp.blogspot.com.br


P: Qual é a programação operacional do servidor??
R: 100%, todos os servidores estão alojados em "a nuvem" (grandes centros de dados em todo o mundo com garantia de tempo de atividade 100%).

P: Como faço para Comprar Valoes?
A: Fale com TioSmoke

P: Minha pergunta não foi respondida aqui, o que devo fazer?
A: faça sua pergunta aqui: https://topcity-rp.blogspot.com.br
]]},
	{ "Veículos", 0 },
	{ "Aluguel de veículos", 1, [[
***********************************************************************
* aluguel de veículos
***********************************************************************

O que é um veículo alugado:
  * Qualquer veículo poide ser gerado  em um marcador branco ou amarelo localizado perto de locais de trabalho ou postos de gasolina, é um veículo de pagamento por minuto que você pode usar para viajar rapidamente ao redor do mapa

Onde faço para encontar um ponto de aluguel?
  * pressione F11 para alternar o mapa grande, procure pequenos quadrados de cor branca ou amarela e encontre o marcador naquele local

Por que não posso gerar um veículo alugado:
  * Se você é procurado por cometer um crime e um responsável pela lei está próximo você não pode gerar

Como faço para retornar o meu veículo:
  * Use /djv ou /rrv, os veículos também são removidos 5 minutos após uma explosão causada por danos ou imediatamente se você sair do servidor.
]] },
	{ "Lojas de veículos / veículos pessoais", 1, [[
***********************************************************************
* Lojas de veículos / veículos pessoais
***********************************************************************

O que é um veículo pessoal:
  * Um veículo que você possui sozinho.

Por que devo comprar quando posso alugar:
   * Um veículo privado pode ser gerado em qualquer lugar no mapa
   * Um veículo próprio pode ser personalizado e todas as suas modificações são salvas
   * O tronco de um veículo próprio pode ser usado para esconder armas da lei, pressione F9 para abrir / fechar o tronco
   * Veículos possuídos não tem taxas de aluguel
   * Você pode gerar uma quantidade ilimitada de veículos Privados, enquanto em um veículo alugado você só pode gerar um veículo no momento

Onde encontro as lojas de carros?
   * Pressione F11 para alternar o mapa grande, procure marcadores de pretos (difíceis de ver) que se parecem com veículos (carros / caminhões / barcos / aeronaves / comboios)

Quais veículos posso comprar:
   * Todos os veículos que estão disponíveis no jogo podem ser comprados, incluindo trens
]] },
	{ "Veículo comandos/binds", 1, [[
***********************************************************************
* Veículo comandos/binds
***********************************************************************

Teclas úteis durante a condução:
   * 'C' - Toggle cruise control
   * 'L' - Alternar faróis do veículo
   * 'H' - Horn / Toggle luzes de emergência do veículo (veículos policiais / camionetas e ambulâncias)
   * ',' - Gire o indicador para a esquerda
   * '. - Gire o indicador para a direita
   * '-' - Alternar luzes de perigo

E os comandos a serem usados durante a condução:

- Abrir/fechar portas de veículos, tudo ou individual
  * /cd\[0-6\]
  * /od\[0-6\]

- setas 
  * /warn
  * /lleft
  * /lright

- Funcionalidades diversas
  * /engine        - Iniciar / parar o motor do veículo
  * /lock          - Bloquear / desbloquear as portas do seu veículo
  * /emlight       - Alternar luzes de emergência
  * /topspeed      - Mostrar a velocidade máxima atual em KM / h
  * /plate <text>  - Alterar o texto da placa
  * /drop          - Saia de um helicóptero com uma corda

- Trazer Carro Alugado
  * /djv - destruir veículo de trabalho
  * /rrv - Trazer veículo de aluguel
]] },
	{ "Trabalhos / Civis", 0 },
	{ "Motorista de ônibus:", 1, [[
***********************************************************************
* Motorista de ônibus:
***********************************************************************

Onde eu encontro esse trabalho?
  * Unity station - Los Santos
  * Pier 69 - San Fierro
  * West side storage building - Las Venturas

O que eu preciso para este trabalho:
  * Um ônibus ou Van, você pode alugar um perto do local de trabalho ou usar seu próprio veículo

O que eu faço neste trabalho:
  * Entre no seu ônibus, selecione a rota que deseja dirigir e siga as instruções na tela.

Quando serei  pago:
  * Toda vez que você fazer uma parada de ônibus ou pega outro jogador como passageiro

Vantagens:
* Maior primeiro, ninguém se preocupa com você no trânsito impune 
* Capacidade de pegar e conhecer muitos novos amigos 
* Um veículo forte com muito combustível para viagens de longa distância
]] },
	{ "Fazendeiro", 1, [[
***********************************************************************
* fazendeiro:
***********************************************************************

Onde eu encontro esse trabalho?
  * Em uma fazenda - (no sul de San Fierro)

O que eu preciso para este trabalho:
* Um trator, uma colheitadeira e dinheiro para as sementes. Você precisa comprar esses veículos ou emprestá-los de um amigo

O que eu faço neste trabalho:
* Digite o trator e dirija ao campo desocupado mais próximo
   * Plante suas sementes com a chave 'N'
   * Deixe suas sementes crescerem, (você pode fazer o que quiser enquanto aguarda, apenas certifique-se de estar lá quando é hora da colheita).
   * Colhe suas sementes com a colheitadeira e pegue os fardos

Quando serei  pago:
  * Cada bala que você escolhe dá dinheiro, apenas certifique-se de que ninguém mais os roube de você

Vantagens:
* Você é um 'caipira', você pode matar qualquer pessoa que se aproxime de suas sementes sem ser procurado 
* Um impulso de dinheiro real para os jogadores que têm dinheiro para investir, sem limitações.
]] },
	{ "Pescador", 1, [[
***********************************************************************
* Fisher:
***********************************************************************

Onde eu encontro esse trabalho?
  * O farol oeste sobre a praia - Los Santos

O que eu preciso para este trabalho:
  * Um barco de qualquer tipo. Você precisa comprar seu barco ou emprestar um de seu amigo

O que eu faço neste trabalho:
   * Dirija o barco lentamente em qualquer água
   * Cruise se você quiser e logo você começará a capturar peixes

Quando serei  pago:
  * Toda vez que você pega um peixe, (ou qualquer outra coisa que possa ser valiosa para esse assunto)

Vantagens:
* Não há muito tráfego no mar, um lugar perfeito para relaxar com seus amigos
   * Possibilidade de se enriquecer rapidamente se tiver sorte
	]] },
	{ "Mineiro de ferro", 1, [[
***********************************************************************
* Iron miner:
***********************************************************************

Onde eu encontro esse trabalho?
  * Na pedreira - Las Venturas

O que eu preciso para este trabalho:
  * Uma quantidade mínima de dinheiro para comprar dinamite 
  * Uma pá, (comprar no painel do trabalho ou em uma loja de ferragens)

O que eu faço neste trabalho:
* Plante uma dinamite usando 'N' então corre rápido como o inferno
   * Pegue a cobertura para a explosão
   * Vá para a área bagunçada e comece a extrair
   * Vender os minerais que você extraiu no grande marcador branco perto do local de trabalho

Quando serei  pago:
* Toda vez que você vende seus minerais

Vantagens:
* Acesso gratuito a dinamite, imagine toda a diversão que você pode fazer com isso;)
]] },
	{ "Mecânico", 1, [[
***********************************************************************
* Mechanic:
***********************************************************************

Onde eu encontro esse trabalho?
  * West side mod shop e pay 'n' spray - Los Santos * Apenas a norte sobre a estação de trem e bombeiros - San Fierro * Norte / Central entre hambúrguer e LVMPD - Las Venturas

O que eu preciso para este trabalho:
  * (Opcional) um reboque

O que eu faço neste trabalho:
  * Pressione X para alternar seu cursor 
  * Clique em um carro danificado * Escolha se deseja reparar ou reabastecer no menu 
  * Toe veículos quebrados para pagar n spray por um pequeno bônus

Quando serei  pago:
  * Todos os reparos ou reabastecimento (exceto os seus próprios veículos) ou se outro jogador derrubar você

Vantagens:
   * Você nunca sofrerá de um carro quebrado no meio do nada
   * Reparos e verificações mais baratas
   * Grande chance de obter gorjeta por outros jogadores
]] },
	{ "Piloto de Avião", 1, [[
***********************************************************************
* Pilot:
***********************************************************************

Onde eu encontro esse trabalho?
   * Aeroporto internacional de Los Santos
   * Aeroporto internacional de San Fierro
   * Aeroporto internacional de Las Venturas

O que eu preciso para este trabalho:
   * Um avião ou helicóptero de propriedade ou alugado, marcadores de aluguel de aeronaves são encontrados perto ou no início dos campos de partida e pouso

O que eu faço neste trabalho:
   * Dirija para o ponto de captação designado e carregue passageiros / carga
   * Voe para o aeroporto de destino (se houver helicóptero ou pequenas aeronaves, mais locais estão disponíveis, muitas vezes mais difíceis de pousar)
   * Terreno com segurança e seja pago

Quando serei  pago:
   * Assim que você chegar ao marcador de entrega e sua aeronave não está muito danificada

Vantagens:
   * Uma maneira rápida de viajar ao redor do estado 
   * Obtém muito dinheiro em comparação com outros empregos do transportador 
   * Obtém um bônus para cada jogador apanhado como passageiro
]] },
	{ "Taxista", 1, [[
***********************************************************************
* Taxi driver:
***********************************************************************

Onde eu encontro esse trabalho?
  * Los Santos international airport
  * San Fierro international airport
  * Las Venturas international airport

O que eu preciso para este trabalho:
  * Um táxi ou taxista, de propriedade ou alugado

O que eu faço neste trabalho:
  * Atravessar até encontrar um passageiro (jogador) ou pegar qualquer passageiro AI
  * "Entregar" o passageiro para a localização solicitada e receber o pagamento

Quando serei  pago:
  * Assim que seu passageiro for entregue com sucesso, se você dirigir um jogador você receberá cada minuto

Vantagens:
  * Ganhe dinheiro dando uma chance a outros jogadores
	]] },
	{ "Maquinista", 1, [[
***********************************************************************
* Train driver:
***********************************************************************

Onde eu encontro esse trabalho?
  * Unity station - Los Santos
  * Cranberry station - San Fierro
  * Yellow Bell station - Las Venturas

O que eu preciso para este trabalho:
  * Um trem (com qualquer quantidade de carros anexados), alugado ou de propriedade

O que eu faço neste trabalho:
  * Dirija o seu trem com segurança ao redor do estado 
  * Entregue passageiros e carga de estação para estação *! Assista os limites de velocidade (consulte a parte inferior direita da tela)

Quando serei  pago:
  * As soon you hit the dropoff destination, the more attached cars the more money you make, but your train is also getting slower during start/stop the more cars you're pulling
  * Players gives an extra bonus payment each minute

Vantagens:
  * Seu veículo é 'imortal' e 'imparável'
  * Pegue e coloque carros ao longo das trilhas
	]] },
	{ "Condutor de Bonde", 1, [[
***********************************************************************
* Tram driver:
***********************************************************************

Onde eu encontro esse trabalho?
  * San Fierro, between the hospital and SFPD (central)

O que eu preciso para este trabalho:
  * Um bonde (com qualquer quantidade de carros anexados), alugado ou de propriedade

O que eu faço neste trabalho:
   * Dirija o (s) seu (s) bonde (s) ao redor das pistas nas ruas de San Fierro
   * Entregar passageiros e jogadores em torno de San Fierro
   *! Assista a outros veículos e jogadores que possam estar no seu caminho
   *! Assista aos limites de velocidade (consulte a parte inferior direita da tela)

Quando serei  pago:
   * Assim que você atingiu o destino do dropoff, quanto mais os carros em anexo ganham mais dinheiro, mas seu trem também está ficando mais lento durante o início / parada dos carros mais que você está puxando
   * Os jogadores recebem um bônus extra a cada minuto

Vantagens:
   * Seu veículo é (quase) imortal e imparável 
   * Pegue e deixe cair carros ao longo das trilhas
	]] },
	{ "Motorista de caminhão", 1, [[
***********************************************************************
* Trucker:
***********************************************************************

Onde eu encontro esse trabalho?
  * The docks - San Fierro
  * The gas station west of Los Santos
  * The docks - Los Santos

O que eu preciso para este trabalho:
  * Um caminhão / van de qualquer tipo, de propriedade ou alugado (semi caminhões precisa de um ou mais reboques para carregar a carga)

O que eu faço neste trabalho:
   * Digite em seu caminhão para ver as rotas /rotas
   * Selecione a rota que deseja dirigir
   * Entregar carga em todo o estado ou dentro da cidade (entregas locais)

Quando serei  pago:
   * Assim que você atingir o destino da entrega, a carga danificada reduzirá seu pagamento, dirija com cuidado!

Vantagens:
   * Seu veículo é (quase) imortal e imparável 
   * Acesse o rádio CB usando: /r <mensagem> (apenas para camioneiros) para rastrear onde os policiais estão verificando os aceleradores
	]] },
	{ "Administração pública", 0 },
	{ "Polícia", 1, [[
***********************************************************************
* Police:
***********************************************************************

Onde eu encontro esse trabalho?
  * LAPD - Los Santos (dentro da sede) 
  * SFPD - San Fierro (dentro da sede) 
  * LVMPD - Las Venturas (dentro da sede) 
  * Dentro de todas as estações do sheriff do lado do país

O que eu preciso para este trabalho:
   * Um cacetete para prender
   * Um tazer para forçar os suspeitos a pararem
   * Outras armas mais fortes, como M4, MP5 e gás lacrimogêneo
   * Um carro rápido e resistente a balas, alugado ou de propriedade (veículos policiais podem ser gerados gratuitamente fora de todos os departamentos de polícia)

O que eu faço neste trabalho:
  * Use /wanted ou /fugitivos listar todos os jogadores desejados e seus locais
   * Patrulha até encontrar qualquer outra coisa suspeita
   * Verifique a parte inferior direita para um texto laranja que indica a distância até o suspeito mais próximo (use essa informação para encontrar o suspeito)
   * Quando você tem um visual no suspeito, pare seu carro, (use as suas sirenes e as luzes para mostrar que você é um policial, pressione 'H' rapidamente para alternar)
   * Uma vez que você parou, o carro dos suspeitos o prende batendo com o seu turnê uma vez
   * Se o suspeito funcionar, dispare-os com o seu tazer (funciona apenas em curta distância)
   * Se o suspeito for violento (ou seja, abre fogo contra você), você pode realizar uma chamada parada de morte onde você atira para matar o suspeito.
   * Uma vez preso, traga o suspeito para o departamento de polícia mais próximo, caminhe para dentro e para baixo no porão para encontrar os blocos de células, caminhe até uma célula de sua escolha para travar o suspeito e ser pago

Quando serei  pago:
   * Quando você mata um suspeito violento, (matar suspeitos não violentos ou pessoas inocentes vai fazer você perder seu emprego)
   * Quando você prendeu alguém e trancou essa pessoa em uma cela
   * (Não Oficiais) Subornos, os jogadores presos podem tentar subornar você, o embuste está permitido aqui, então não se esqueça do que está fazendo, exija o dinheiro primeiro

Vantagens:
   * Acesso a luzes de emergência (pressione 'H' rapidamente para alternar) em qualquer veículo
   * Acesso a todos os veículos de aplicação da lei
   * Capacidade de prender criminosos
   * Capacidade de ser pago instantaneamente por matar um jogador violento (consulte /wanted e certifique-se de que o jogador que deseja matar é marcado como vermelho e não laranja ou qualquer outra cor)
]] },
	{ "Guarda prisional", 1, [[
***********************************************************************
* Prison guard:
***********************************************************************

Onde eu encontro esse trabalho?
  * Veja "Polícia"

O que eu preciso para este trabalho:
  * Veja "Polícia"

O que eu faço neste trabalho:
  * Viaje para a prisão localizada no lado oeste da Marina de Bayside, (norte de San Fierro)
  * Guarda os prisioneiros, sentado por uma torre com um atirador ou cruzando com um helicóptero
  * Mate alguém tentando escapar
  * Verifique os visitantes e deixe-os entrar / sair Organizar atividades para os prisioneiros
  * Pare os tumultos jogando lagrimas ou taze os prisioneiros

Quando serei  pago:
  * Você não está sendo pago em dinheiro, só (quase) sadismo livre para os prisioneiros

Vantagens:
  * Acesso gratuito a helicópteros blindados e barcos
  * Você é responsável pelos prisioneiros e seu trabalho é obrigá-los a obedecer você
]] },
	{ "Chefe de policia", 1, [[
Os chefes da polícia garantem que os jogadores que trabalham para a aplicação da lei respeitem as próprias leis e tenham a capacidade de proibir os jogadores de participarem de atividades relacionadas à lei como responsáveis pela lei, ninguém pode ser chefe da polícia, você deve enviar um pedido no fórum para conceder acesso,


Para adicionar ou remover um novo chefe da polícia (apenas administradores)
  * /addpc <nickname>
  * /removepc <nickname>

Para proibir ou destituir um encarregado da lei (como chefe da polícia)
  * /banfromlaw <nickname>
  * /revokebanfromlaw <nickname>
]] },
	{ "Códigos policiais", 1, [[
Uma lista de códigos policiais usados em situações de jogo de papéis:

10-4 Roger que
10-10 Off duty
10-14 Fornecer Escort
10-15 Preso em custódia
10-23 Stand by
10-29 Check For Wanted (pessoa, veículo ou objeto)
10-97 Chegou na cena
10-98 Atribuição concluída

11-10 Tome um relatório
11-24 Veículo abandonado
11-25 Risco de trânsito
11-41 Ambulância Necessária
11-66 Sinais de trânsito defeituosos
11-78 Paramédicos Despachados
11-79 Acidente-ambulância rolando
11-98 Oficial Requer Ajuda, Emergência]]},
	{ "Como prender", 1, [[
Há duas maneiras:

A: Assassinato:
   * mata um jogador violento para prendê-lo / ela

B: prisão tradicional
   * Pressione o suspeito com o seu turno de papel uma vez
   * Se ele insistir em correr, atire-o com seu tazer para detê-lo primeiro
]] },
	{ "Como eu sei quem é procurado?", 1, [[
Use o comando /wanted obter uma lista de jogadores procurados
   * "tempo violento" é o tempo restante em que você pode realizar uma prisão de morte
   * "nível desejado" indica o quão "quente" é um suspeito, o nível mais procurado é o pagamento mais que você obtém

O que as cores significam:
   * Os nomes que aparecem em vermelho são jogadores "violentos", que podem ser mortos.
   * Os nomes que aparecem em laranja são "não violentos", você só pode realizar uma prisão tradicional neles
   * Os nomes que aparecem em amarelo são jogadores presos, você só pode proteger o responsável pela lei segurando o suspeito
   * Os nomes que aparecem em verde estão na prisão, (o tempo é visto aqui também expresso como segundos restantes)
]] },
	{ "Chat Policial", 1, [[
Law chat é um bate-papo multi-equipe visível para o governo e serviço de emergência

  * Use: /e <message>
]] },
	{ "Equipe do servidor", 1, [[
Leia mais sobre o pessoal do servidor e os diferentes tipos de pessoal neste link:
https://site em contrução

Informe a equipe ou jogadores que violam as regras do servidor neste link:
https://site em contrução
]] },
	{ "Criminal", 0 },
	{ "Como faço para se tornar um criminoso", 1, [[
Existem três formas comuns de se tornar um criminoso:
   * Por comando: /criminal
   * Por GUI: pressione F5, depois vá criminal (pressione F5 novamente para fechar a GUI)
   * Ao cometer um crime, ou seja, quando o seu nível desejado aumentar rapidamente ou acima de um limite
]] },
	{ "Como eu roubo uma loja?", 1, [[
Entre em qualquer loja e alvejeie uma arma no caixa, se você pretende usar uma arma de meele ou seus punhos você deve ficar perto do caixa antes de começar o assalto.

Existem muitas lojas diferentes que podem ser roubadas
   * 17 Lojas de pele
   * 34 restaurantes de fast food
   * 3 Lojas de ferragens
   * 11 Ammu nation (sob seu próprio risco)
]] },
	{ "Missões de seqüestro", 1, [[
Procure o flag da bandeira em seu mapa, localize o carro nesse local e entre, entregue o carro para a localização da dropoff e receba o pagamento. Quanto mais criminosos perseguem o carro, o dinheiro que você ganha]] },
	{ "Bolsas de mistério", 1, [[
Procure o ponto de interrogação no mapa, localize e pegue o saco naquele local, ele contém uma feliz surpresa para os criminosos.]] },
	{ "Turfs", 1, [[
As áreas coloridas no mapa são turfs, você pode capturar um território entrando como um criminoso e membro de uma gangue, (pressione F6 para participar ou criar um). Depois de entrar no relvado, ele começará a piscar se os requisitos acima forem cumpridos. Você pode ver o tempo que você tem para proteger o relvado na parte inferior da tela, durante esse período você não pode morrer ou sair do relvado, se você o fizer, você vai perder.

Turfs dá-lhe dinheiro a cada 10 minutos enquanto está online. Quanto mais turfe você possui, mais dinheiro ganha.

Requisitos:
   * Estar no time criminoso ou gangster
   * Ser membro de uma gangue
]] },
	{ "Gangsters", 1, [[
Os jogadores que morrem em um relvado serão transferidos para a equipe do gângster, ele funciona do mesmo modo que a equipe criminal, embora os criminosos e gângsteres não possam ver os golpes do eachothers no mapa, por isso é uma ótima oportunidade para um ataque furtivo. Gangsters pode, em qualquer momento, tornar-se um criminoso novamente por comando ou pressionando F5.
]] },
	{ "Como reduzir o nível de procurado/wanted", 1, [[
Há duas maneiras de roubar seu nível desejado:
   * Permanecendo longe de policiais, o texto que mostra o nível desejado no hud será verde se reduzir e vermelho se aumentar, o branco significa que você é violento eo nível desejado permanece constante até o momento violento ter contado com sucesso
   * Pagando suas multas, use / multa para ver quanto custa e siga as instruções em sua tela para aceitar ou rejeitar, você não pode pagar uma multa quando um agente de leis está nas proximidades.

Já preso? não se preocupe, ainda pode haver esperança:
   * Oferecer para subornar o policial que o prendeu, isso é arriscado e eles podem roubar você, é proibida a fraude durante Suborno

Na cadeia? não se preocupe, ainda pode haver esperança:
   * Você pode escapar da prisão, veja a seção "Cadeia" para mais detalhes
]] },
	{ "Prisão", 1, [[
A prisão está localizada na sua própria ilha
pequenas ilhas da rocha atrás das montanhas, a oeste, sobre Bayside
Marina, este é um lugar cheio de esconderijos, um lugar onde policiais
e outras unidades de lei podem se tornar guardas da prisão e matar qualquer criminoso
tentando escapar.

7 torres de guarda com foguetes dispararão em qualquer coisa chegando nas proximidades
exceto para unidades de lei, a menos que estejam tentando ajudar um criminoso
escape. O local só é acessível por barco ou helicóptero e
Os guardas da prisão têm múltiplos criadores para esses veículos ao redor
cadeia

Para escapar, você precisa se afastar da prisão, despercebido e
sobreviver a foguetes e potenciais guardas que possam notar seu
tentativa de escape, uma tentativa de escape bem sucedida irá ajudá-lo 10
estrelas, (tornando-se o pior crime que você pode cometer).

Note que suas armas serão devolvidas quando você for lançado,
mas não se você escapou da prisão durante a sessão.]] },
	{ "Lojas", 0 },
	{ "Fast food restaurantes", 1, [[
Se você quiser recuperar a saúde, mas está longe de um hospital ou
só quero ficar gordo, temos 32 restaurantes de fast food para você, todos
Clucki'n Bells, Burger shots e Well stack Pizza C.o estão abertos
para você 24/7. Bem vinda.

Você é preguiçoso? Experimente o novo disco através da maioria dos tiros de burger.]] },
	{ "Ammu nation", 1, [[
Proteja-se com uma arma, a maneira patriótica. Ammu nation é o
melhor lugar para comprar armas baratas com 11 lojas ao redor do mapa, lá
também são duas lojas especiais onde você pode comprar as coisas pesadas, como
Heat buscando foguetes, bombas e granadas.]] },
	{ "Lojas de roupas e Skins", 1, [[
Seis cadeias diferentes e 17 lojas em todo o mapa, caso queira
para mudar sua pele ou apenas dar uma nova aparência.]] },
	{ "Posto de gasolina", 1, [[
Compre combustível para seus veículos aqui.]] },
	{ "Pay'n'spray", 1, [[
O seu carro está danificado? Então entre e nós o reparamos por você por um
preço justo, muito mais barato do que exigir um mecânico.]] },
	{ "Mod shops", 1, [[
Quer modificar seu carro? Temos todo o equipamento que você precisa,
hidráulica, nitros, novas cores, spoilers e outras coisas agradáveis.]] },
	{ "Máquinas de venda automática", 1, [[
If you see a vending machine walk up to it and press 'Enter' to buy
snacks and regain health.Se você vir uma máquina de venda automática e pressionar 'Enter' para comprar
lanches e recuperação de saúde.]] },
	{ "Lojas de ferragens", 1, [[
Se você precisar de um baseball mal, clube de golfe, faca ou outras ferramentas, nós
tê-los aqui.]] },
	{ "Recursos variados", 0 },
	{ "Lista de comando", 1, [[
:: Animações ::
Key 'F10'   -- Abre uma GUI com todas as animações disponíveis
/chat       -- Expressões de rosto para conversar
/cpr        -- Faça um RCP em outro jogador
/deal1      -- Se você quer se parecer com um traficante de drogas
/deal2
/deal3
/fatidle    -- Parece uma pessoa gorda
/getup      -- Suba do chão
/getupfront -- Suba para a frente do chão
/gum        -- Pegue uma goma
/gumchew    -- Mastique uma goma
/handcower  -- Tire a tampa usando as mãos
/handsup    -- Mãos para seguir as ordens de um
               Policial
/lean       -- Incline-se em direção a uma parede ou a um carro
/lookaround -- Olhe ao redor no lugar
/mourn      -- Parece triste (se alguém morrer talvez)
/sit        -- Sente-se em qualquer coisa que você deseja se sentar
/smoke      -- Fumar um cigarro
/taxiwink   -- Wink em um táxi
/tired      -- Parece cansado
/turn180    -- Inversão de marcha
/urp        -- Abra a boca
/wave       -- Wave para outro jogador
	*Todas as animações podem ser interrompidas ao avançar a qualquer momento

:: Geral (Teclas) ::
F1               -- Obter Ajuda
F2               -- Como a GUI do veículo onde você
                    pode gerenciar seus próprios veículos.
F3 | /stats      -- Mostrar a janela das estatísticas
F4               -- Usar drogas
F5               -- Mostre a GUI de gerenciamento de tarefas, trabalho atual
                     Estatísticas e botões rápidos para terminar seu trabalho
                     ou se tornar um criminoso.
/endwork
/criminal        -- (Ver GUI de gerenciamento de tarefas)
F6               -- Abra o sistema de grupo, crie um grupo / grupo e convide pessoas, útil para virar ou se você quer apenas conversar em particular.
F7 (admin)       -- O painel de administração (apenas funcionários)
P (admin) - O painel de administração (apenas funcionários)
F8 - Mostrar o console
F9 - (veículo próximo) abre o porta-malas do veículo
F10 - Todas as animações disponíveis.
F11 - O mapa sobre San Andreas
F12 - Tire uma captura de tela
B - Mostra o teu telefone

:: Banco ::
/give(transf) <player_nick> <amount>     - Transferir dinheiro para outro
                                      bolso do jogador para o bolso
:: Polícia ::
/e <mensagem>      - Chat de emergência e ocorencias
/warn          	   - Mostra uma lista de todos os jogadores procurados
/release           - Libere um suspeito que você está segurando

:: Criminals ::
/fine              - Tentativas de pagar uma multa para se livrar
                       do seu nível desejado. Atenção! Está
                       caro e pode falhar.
/sell              - Comece / pare de vender drogas

:: Transporte público ::
/taxi              - Chamar Taxi
/bus               -- Deixe os motoristas de ônibus saberem que você está esperando
/pilot             -- Ligue para um piloto de helicóptero
/train             -- Deixe os condutores saberem que você está esperando
/tram              -- Deixe os motoristas de bonde saberem que você está esperando

:: Staff ::
/gv                -- Gerar Veiculo
/setpos x y z      -- Teleporte para qualquer cordenada
/mute <player_nick> <time_in_minutes> <reason>
/jail <player_nick> <time_in_seconds> <reason>
/ban <player_nick> <time_in_minutes> <reason>
]]},
	{ "Sistema de casa", 1, [[
Compre uma casa como um lugar para relaxar, um lugar para se esconder da lei, esconder
seu dinheiro e armas ou como base para sua gangue. As possibilidades
são infinitas. As casas estão disponíveis em uma ampla gama de preços ao
mapa, a partir de apenas 500 $ para um galpão de campo até 1.000'000 $
para uma manson de luxo no norte de Los Santos ou no centro de San Fierro e
tudo entre.

Você também pode alugar sua casa como senhorio ou alugar de outra pessoa,
Tudo isso será configurado na GUI da casa, que é simples de
entenda, digite: / househelp para mais informações ou pergunte a outra pessoa.]] },
	{ "O telefone", 1, [[
Pressione 'B' para abrir o seu telefone nokia, que você pode usar para ouvir
rádio, envie SMS para outros jogadores ou solicite serviços. ]] },
	{ "Exercício", 1, [[
Evite dirigir seu carro nas ferrovias, você pode ser atingido por um
treinar, você também pode ver bondes em San Fierro, então fique atento ao dirigir
Além disso, eles não vão parar para você, trens e bondes também são ótimos
maneira de viajar ao redor do mapa de graça.]] },
	{ "Sistema de negócios", 1, [[
Há muitas propriedades de negócios para investir em todo o mapa, um
O início do negócio gerará dinheiro depois de cerca de duas semanas, depois disso
O tempo que produziu tanto dinheiro como você pagou por comprá-lo. Busniess
estão disponíveis em todas as classes de preços, mas podem ser difíceis de encontrar o pensamento.]] },
	{ "Sistema bancário", 1, [[
Cofre seu dinheiro, colocando-os no banco, caixas eletrônicos e bancos são todos
útil para depositar, retirar ou enviar dinheiro para outros jogadores. Caixas eletrônicos e
Os bancos estão marcados com um sinal de dólar no mapa.]] },
	{ "Hospitais", 1, [[
Existem muitos hospitais ao redor do mapa, você pode recuperar a saúde em Nay
Hospital rápido e por um preço justo, você também irá respawn no mais próximo
hospital se você morrer. Os hospitais são zonas seguras e é estritamente proibido
para matar ou danificar pessoas ou veículos perto de um hospital.]] },
}
