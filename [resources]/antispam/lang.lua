
-- Definition of default language for this resource
r_lang = exports.core:getLanguage() or "en_US"

-- Language text storage for this resource
lang_txt = {
	-- English/English
	["en_US"] = {
		["msg_no_spam"] 	= "Do not spam commands!",
		["log_cmd_issuer"]	= " issued the server command: '",
	},

	-- Swedish/Svenska
	["sv_SE"] = {
		["msg_no_spam"] 	= "Kommando spam är ej tillåtet!",
		["log_cmd_issuer"]	= " utförde följande kommando: '",
	},
	
	-- Spanish/Español
	["es_ES"] = {
		["msg_no_spam"] 	= "No hagas spam de comandos!",
		["log_cmd_issuer"]	= " ejecutó el siguiente comando: '",
	},
}
