--[[
********************************************************************************
	Project owner:		RageQuit community
	Project name: 		TopCity-RP
	Developers:   		Mr_Moose

	Source code:		https://github.com/404rq/TopCity-RP/
	Bugtracker: 		https://discuss.404rq.com/t/issues
	Suggestions:		https://discuss.404rq.com/t/development

	Version:    		Open source
	License:    		BSD 2-Clause
	Status:     		Stable release
********************************************************************************
]]--

--[[ Show how much minerals an ironminer has ]]--
function show_minerals_info( )
	if getElementData( localPlayer, "Occupation" ) == "Iron miner" then
		local mineral_string = "Minerios: Pt: "..(getElementData(localPlayer, "ironminer.platinum") or 0)..
    		" | Au: "..(getElementData(localPlayer, "ironminer.gold") or 0)..
    		" | Ag: "..(getElementData(localPlayer, "ironminer.silver") or 0)..
    		" | Fe: "..(getElementData(localPlayer, "ironminer.iron") or 0)..
    		" | Cu: "..(getElementData(localPlayer, "ironminer.cupper") or 0)..
    		" | R$"..math.floor(getElementData(localPlayer, "ironminer.profit") or 0)
		local sx, sy = guiGetScreenSize( )
		dxDrawText(mineral_string, (sx/2)-278, sy-28, 0, 0,
			tocolor(0, 0, 0, 255 ), 1, "default-bold" )
		dxDrawText(mineral_string, (sx/2)-280, sy-30, 0, 0,
			tocolor(200, 200, 200, 255 ), 1, "default-bold" )
	end
end
addEventHandler("onClientRender", root, show_minerals_info)

--[[ Update a value determining if a player is on the ground ]]--
local is_on_ground,old_check = false,true
function is_player_on_the_ground( )
	local px, py, pz = getElementPosition(localPlayer)
	--outputConsole(getGroundPosition(px, py, pz + 10).." "..pz.." "..(getGroundPosition(px, py, pz + 10) + 2))
	if getGroundPosition(px, py, pz + 10) < pz and (getGroundPosition(px, py, pz + 10) + 2) > pz then
		is_on_ground = true
	else
		is_on_ground = false
	end

	if is_on_ground ~= old_check then
		setElementData(localPlayer, "isOnGround", is_on_ground)
		old_check = is_on_ground
	end
end

--[[ Start checking if a player is on the ground ]]--
is_player_on_the_ground()
setTimer(is_player_on_the_ground, 3000, 0)
