The core system of GTW law and wanted level system, by splitting up and rewriting the old complex system
into 4 new resources with this as the core system managing exports it's finally here, law jobs are assigned 
by civilians, the team is "Government" and the occupation could be anything, special features can be
restricted to specific occupations. Vehicles are managed by vehicles but the rest is included.
 
wanted will manage wanted level and tell you who you can arrest or not.<br>
jail and jailmap will manage everything related to jail.

## Functions available

`int dist = distanceToCop(player crim)` _(server)  
Get the distance from a criminal element (player) to nearest law enforcer._

`player cop = nearestCop(player crim)` _(server)  
Get the nearest law enforcer as player element._

`bool isArrested = isArrested(player crim)` _(server)  
Returns true if the criminal is arrested, false otherwise._

## Exported functions

`int dist = distanceToCop(player crim)` _(server)  
Get the distance from a criminal element (player) to nearest law enforcer._

`player cop = nearestCop(player crim)` _(server)  
Get the nearest law enforcer as player element._

`bool isArrested = isArrested(player crim)` _(server)  
Returns true if the criminal is arrested, false otherwise._

## Requirements

topbar<br>
wanted<br>
jail<br>
jailmap (required if using "jail")<br>
skin_shop (optional)<br>
policechief (optional)<br>
vehicles *(optional, (_coming soon_))<br>
