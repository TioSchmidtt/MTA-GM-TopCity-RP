--[[
********************************************************************************
	Project owner:		RageQuit community
	Project name: 		TopCity-RP
	Developers:   		Mr_Moose

	Source code:		https://github.com/404rq/TopCity-RP/
	Bugtracker: 		https://discuss.404rq.com/t/issues
	Suggestions:		https://discuss.404rq.com/t/development

	Version:    		Open source
	License:    		BSD 2-Clause
	Status:     		Stable release
********************************************************************************
]]--

--[[ Calculate the ID for next DeliveryPoint and inform the passengers ]]--
function create_new_delivery_point(plr, ID, load_time)
	-- There must be a route at this point
	if not plr or not getElementData(plr, "trucker.currentRoute") then return end

	-- Get the coordinates of the next stop from our table
	local x,y,z = truck_routes[getElementData(plr, "trucker.currentRoute")][ID][1],
		truck_routes[getElementData(plr, "trucker.currentRoute")][ID][2],
		truck_routes[getElementData(plr, "trucker.currentRoute")][ID][3]

	local msg_end_text = "pegar sua carga"
	if ID > 1 then msg_end_text = "para terminar a sua entrega" end

	-- Alright, we got some passengers, let's tell them where we're going
	exports.topbar:dm("Caminhoneiro: dirigir para: "..getZoneName(x,y,z)..
		" em "..getZoneName(x,y,z, true).." "..msg_end_text, plr, 255,100,0)

	-- Tell the client to make a marker and a blip for the truckdriver
	triggerClientEvent(plr, "trucker.createDeliveryPoint", plr, x,y,z)

	-- Save the stop ID
	setElementData(plr, "trucker.currentStop", ID)
end

--[[ Drivers get's a route asigned while passengers has to pay when entering the truck ]]--
function on_truck_enter(plr, seat, jacked)
	-- Make sure the vehicle is a truck
        if not truck_vehicles[getElementModel(source)] then return end
	if getElementType(plr) ~= "player" then return end

	-- Whoever entered the truck is a truckdriver
	if getPlayerTeam(plr) and getPlayerTeam(plr) == getTeamFromName("Civilians") and
		getElementData(plr, "Occupation") == "Trucker" and
		seat == 0 and not getElementData(plr, "trucker.currentRoute") then

		-- Let him choose a route to drive
		triggerClientEvent(plr, "trucker.selectRoute", plr)
	elseif getPlayerTeam(plr) and getPlayerTeam(plr) == getTeamFromName("Civilians") and
		getElementData(plr, "Occupation") == "Trucker" and
		seat == 0 and getElementData(plr, "trucker.currentRoute") then
		start_new_route(getElementData(plr, "trucker.currentRoute"), plr)
	end
end
addEventHandler("onVehicleEnter", root, on_truck_enter)

--[[ A new route has been selected, load it's data ]]--
function start_new_route(route, plr)
	if not client then client = plr end
    	setElementData(client, "trucker.currentRoute", route)
	if not getElementData(client, "trucker.currentStop") then
		create_new_delivery_point(client, 1)
	else
		create_new_delivery_point(client, getElementData(client, "trucker.currentStop"))
	end
end
addEvent("trucker.selectRouteReceive", true)
addEventHandler("trucker.selectRouteReceive", root, start_new_route)

--[[ Calculate the next stop ]]--
function calculate_next_stop(tr_payment, load_time)
	-- Make sure the player is driving a truck
	if not isPedInVehicle(client) then return end
	if not truck_vehicles[getElementModel(getPedOccupiedVehicle(client))] then return end

	-- Calculate the payment minus charges for damage
	local fine = math.floor(tr_payment -
		(tr_payment*(1-(getElementHealth(getPedOccupiedVehicle(client))/1000))))

	-- Increase stats by 1
	local playeraccount = getPlayerAccount( client )
	local delivery_points = (exports.core:get_account_data( playeraccount, "GTWdata.stats.delivery_points" ) or 0) + 1
	exports.core:set_account_data( playeraccount, "GTWdata.stats.delivery_points", delivery_points )

	-- Pay the driver
	givePlayerMoney(client, fine + math.floor(delivery_points/4))
	exports.topbar:dm("Trucker: Delivery finished, you earned: $"..tostring(fine + math.floor(delivery_points/4)),
		client, 0, 255, 0)

	-- Notify about the payment reduce if the truck is damaged
	if math.floor(tr_payment - fine) > 1 then
		takePlayerMoney(client, math.floor(tr_payment - fine))
		exports.topbar:dm("Removido $"..tostring(math.floor(tr_payment - fine)).." devido a danos no seu caminhão!",
			client, 255, 0, 0)
	end

	-- Get the next destination on the list
	if #truck_routes[getElementData(client, "trucker.currentRoute")] ==
	 	tonumber(getElementData(client, "trucker.currentStop")) then
		-- Mission are finished, let's select a new Mission
		setElementData(client, "trucker.currentStop", 1)
	else
		setElementData(client, "trucker.currentStop", tonumber(
			getElementData(client, "trucker.currentStop" ))+1)
	end

	-- Force the client to create markers and blips for the next stop
	setTimer(create_new_delivery_point, load_time, 1, client, tonumber(getElementData(client, "trucker.currentStop")))
end
addEvent("trucker.payTrucker", true)
addEventHandler("trucker.payTrucker", resourceRoot, calculate_next_stop)

--[[ A little hack for developers to manually change the route ID ]]--
function set_manual_stop(plr, cmd, ID)
	local is_staff = exports.staff:isStaff(plr)
	if not is_staff then return end
	setElementData(plr, "trucker.currentStop", tonumber(ID) or 1)
	create_new_delivery_point(plr, tonumber(ID) or 1)
end
addCommandHandler("setDeliveryPoint", set_manual_stop)

--[[ A little hack for developers to manually change the route ID ]]--
function choose_route(plr, cmd, ID)
	-- Make sure it's a trucker inside a truck requesting this
	if getPlayerTeam(plr) and getPedOccupiedVehicle(plr) and
		getPlayerTeam(plr) == getTeamFromName("Civilians") and
		getElementData(plr, "Occupation") == "Trucker" and
		getPedOccupiedVehicleSeat(plr) == 0 and truck_vehicles[getElementModel(getPedOccupiedVehicle(plr))] then

		-- Force selection of new route
		triggerClientEvent(plr, "trucker.selectRoute", plr)
	end
end
addCommandHandler("route", choose_route)
addCommandHandler("rota", choose_route)
addCommandHandler("rotas", choose_route)
addCommandHandler("routes", choose_route)
addCommandHandler("routelist", choose_route)
addCommandHandler("routeslist", choose_route)

--[[ Display a delayed message securely ]]--
function delayed_message(text, plr, r,g,b, color_coded)
	if not plr or not isElement(plr) or getElementType(plr) ~= "player" then return end
	if not getPlayerTeam(plr) or getPlayerTeam(plr) ~= getTeamFromName("Civilians") or
		getElementData(plr, "Occupation") ~= "Trucker" then return end
	if not getPedOccupiedVehicle(plr) or not truck_vehicles[getElementModel(getPedOccupiedVehicle(plr))] then return end
	exports.topbar:dm(text, plr, r,g,b, color_coded)
end
