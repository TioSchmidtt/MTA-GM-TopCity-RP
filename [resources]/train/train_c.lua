--[[
********************************************************************************
	Project owner:		RageQuit community
	Project name: 		TopCity-RP
	Developers:   		Mr_Moose

	Source code:		https://github.com/404rq/TopCity-RP/
	Bugtracker: 		https://discuss.404rq.com/t/issues
	Suggestions:		https://discuss.404rq.com/t/development

	Version:    		Open source
	License:    		BSD 2-Clause
	Status:     		Stable release
********************************************************************************
]]--

--[[ Destroys a train as soon it streams out and no other players are nearby ]]--
function stream_out_train( )
	triggerServerEvent("train.destroy", root, source)
end

--[[ Check when the train streams out and destroys it ]]--
function check_stream_out(c_train)
    	addEventHandler("onClientElementStreamOut", c_train, stream_out_train)
	--setElementStreamable(c_train, false)
end
addEvent("train.onStreamOut", true)
addEventHandler("train.onStreamOut", root, check_stream_out)

--[[ Updating control state based on server orders ]]--
function set_train_control_policy(engineer, state)
	if state == 0 then
		setPedControlState(engineer, "accelerate", false)
		setPedControlState(engineer, "brake_reverse", false)
	elseif state == 1 then
		setPedControlState(engineer, "accelerate", true)
		setPedControlState(engineer, "brake_reverse", false)
	elseif state == 2 then
		setPedControlState(engineer, "accelerate", false)
		setPedControlState(engineer, "brake_reverse", true)
	end
end
addEvent("train.setPedControlState", true)
addEventHandler("train.setPedControlState", root, set_train_control_policy)

function set_train_track(cmd, new_track)
	-- Verify MTA version
	local version = getVersion()
	if tonumber(version.mta) < 1.6 then
		exports.topbar:dm("Este recurso ainda não é suportado! faça o download do MTASA v1.6 ou posterior", 255,0,0)
		return
	end

	-- Check if the player is driving a train
	local train = getPedOccupiedVehicle(localPlayer)
	if not train or getVehicleType(train) ~= "Train" then return end
	local x,y,z = getElementPosition(train)

	if cmd == "gettrack" then
		exports.topbar:dm("A faixa atual é: "..getTrainTrack(train), 255,100,0)
	elseif new_track then
		setTrainTrack(train, new_track)
		setElementPosition(train, x,y,z)
		exports.topbar:dm("A faixa atual é: "..getTrainTrack(train), 255,100,0)
	end
end
addCommandHandler("settrack", set_train_track)
addCommandHandler("gettrack", set_train_track)
