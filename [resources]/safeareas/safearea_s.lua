--[[
********************************************************************************
	Project owner:		RageQuit community
	Project name: 		TopCity-RP
	Developers:   		Mr_Moose

	Source code:		https://github.com/404rq/TopCity-RP/
	Bugtracker: 		https://discuss.404rq.com/t/issues
	Suggestions:		https://discuss.404rq.com/t/development

	Version:    		Open source
	License:    		BSD 2-Clause
	Status:     		Stable release
********************************************************************************
]]--

-- Redcue damage made by minigun
setWeaponProperty(38, "pro", "damage", 2)
setWeaponProperty(38, "std", "damage", 2)
setWeaponProperty(38, "poor", "damage", 2)

-- Reduce health when falling into cold water
setTimer(function()
	for k,v in pairs(getElementsByType("player")) do
		if isElementInWater(v) and not getPedOccupiedVehicle(v) then
			local health = getElementHealth(v)
			local new_health = health - math.random(4, 12)
			local x,y,z = getElementPosition(v)
			-- Check if the area is north west (SF), south LS or if it's night
			local hour, minutes = getTime()
			if (y > 1500 and x < 1000) or y < -2800 or hour > 22 or hour < 9 then
				if new_health > 0 then
					setElementHealth(v, new_health)
					exports.topbar:dm("Esta água está fria! Levante-se antes de morrer!", v, 255, 0, 0)
				else
					killPed(v)
					exports.topbar:dm("Você congelou até a morte na água fria", v, 100, 255, 100)
				end
			end
		end
	end
end, 4000, 0)

addCommandHandler("gtainfo", function(plr, cmd)
	outputChatBox("[TopCity-RP] "..getResourceName(
	getThisResource())..", by: "..getResourceInfo(
        getThisResource(), "author")..", v-"..getResourceInfo(
        getThisResource(), "version")..", is represented", plr)
end)
