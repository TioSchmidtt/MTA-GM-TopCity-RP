local sWidth, sHeight = guiGetScreenSize()
function initialize_skin_shop()
	window = exports.UI:createWindow(sWidth-340, (sHeight-500)/2, 340, 500, "Loja de Skin", false)
	skin_list_gui = guiCreateGridList(10, 25, 320, 425, false, window)
	col_name = guiGridListAddColumn(skin_list_gui, "Nome", 0.45)
	col_id = guiGridListAddColumn(skin_list_gui, "ID", 0.2)
	btn_buy = guiCreateButton(10, 456, 158, 40, "Comprar skin (R$500)", false, window)
	btn_close = guiCreateButton(172, 456, 158, 40, "Fechar", false, window)
	guiSetVisible(window, false)

	-- Apply GTW GUI styles
	exports.UI:setDefaultFont(skin_list_gui, 10)
	exports.UI:setDefaultFont(btn_buy, 10)
	exports.UI:setDefaultFont(btn_close, 10)

	-- Events for the buttons
	addEventHandler("onClientGUIClick", btn_close, function( )
		guiSetVisible(window, false)
		exports.UI:showGUICursor(false)
		setPlayerHudComponentVisible("all", true)
		setElementModel(localPlayer, model)
		setElementFrozen(localPlayer, false)
	end, false)
	addEventHandler("onClientGUIClick", btn_buy, buy_the_skin, false)
	addEventHandler("onClientGUIClick", skin_list_gui, preview_skin, false)
end
addEventHandler("onClientResourceStart", resourceRoot, initialize_skin_shop)

function show_skin(skinsTable)
	if getPlayerMoney() < 20 then exports.topbar:dm("Você não pode comprar esta Skin", 255, 0, 0) return end
	if getPlayerTeam(localPlayer) ~= getTeamFromName("Unemployed") and
		getPlayerTeam(localPlayer) ~= getTeamFromName("Criminals") and
		getPlayerTeam(localPlayer) ~= getTeamFromName("Gangsters") then
	 	exports.topbar:dm("Termine o seu trabalho antes de mudar sua pele, clique em F5 ou use /sairtrab", 255, 0, 0) return end
	if getElementData(localPlayer, "rob") then exports.topbar:dm("Você não pode comprar enquanto rouba seu idiota!", 255, 0, 0) return end
	guiGridListClear(skin_list_gui)
	setElementFrozen(localPlayer, true)
	for category, skins in pairs(skinsTable) do
		local row = guiGridListAddRow(skin_list_gui)
		guiGridListSetItemText(skin_list_gui, row, 1, category, true, false)
		for id, name in pairs(skins) do
			local row = guiGridListAddRow(skin_list_gui)
			guiGridListSetItemText(skin_list_gui, row, 1, name, false, false)
			guiGridListSetItemText(skin_list_gui, row, 2, id, false, false)
		end
	end
	guiSetVisible(window, true)
	exports.UI:showGUICursor(true)
	local rx,ry,rz = getElementRotation(localPlayer)
	fadeCamera(false)
	setTimer(setCameraTarget, 800, 1, localPlayer)
	setTimer(setElementRotation, 1000, 1, localPlayer, rx,ry, rz-180)
	setTimer(fadeCamera, 1000, 1, true)
	model = getElementModel(localPlayer)
end
addEvent("skin_shop.show_skin", true)
addEventHandler("skin_shop.show_skin", root, show_skin)

function preview_skin()
	local row = guiGridListGetSelectedItem(skin_list_gui)
	if (not row or row == -1) then return end
	local id = guiGridListGetItemText(skin_list_gui, row, 2)
	id = tonumber(id)
	if (not id) then return end
	setElementModel(localPlayer, id)
end

function buy_the_skin()
	local row = guiGridListGetSelectedItem(skin_list_gui)
	if (not row or row == -1) then return end
	local id = guiGridListGetItemText(skin_list_gui, row, 2)
	id = tonumber(id)
	if (not id) then return end
	setElementModel(localPlayer, model)
	setElementFrozen(localPlayer, false)
	guiSetVisible(window, false)
	exports.UI:showGUICursor(false)
	triggerServerEvent("skin_shop.buy_the_skin", root, id)
end
