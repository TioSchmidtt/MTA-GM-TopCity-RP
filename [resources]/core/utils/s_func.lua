
function display_total_playtime(plr)
	local account_table = getAccounts()
	local total_account_time = 0
	for w,acc in ipairs(account_table) do
		total_account_time = total_account_time +(exports.core:get_account_data(acc, "GTWdata.playtime") or 0)
	end
	local hour = tostring(math.floor(total_account_time/(1000*60*60)))
	local minute = tostring(math.floor(total_account_time/(1000*60))%60)
	local nMinute = math.floor(total_account_time/(1000*60))%60
	if nMinute < 10 then
	   	minute = "0"..minute
	end
	outputChatBox("Tempo total: "..tostring(hour).."h, "..tostring(minute).."m, in: "..tostring(#account_table).." accounts", plr, 255, 255, 255)
end
addCommandHandler("accounts-info", display_total_playtime)
addCommandHandler("time", display_total_playtime)
addCommandHandler("conta-info", display_total_playtime)

--[[ Convert number e.g 100000 -> 100.000 ]]--
function convert_num(number)
	local formatted = number
	while true do
		formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
		if ( k==0 ) then
			break
		end
	end
	return formatted
end
