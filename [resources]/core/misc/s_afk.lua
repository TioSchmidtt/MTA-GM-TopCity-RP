-- List of AFK players
is_afk = { }

--[[ Toggle afk mode ]]--
function go_afk(plr)
	if not is_afk[plr] and getPlayerWantedLevel(plr) == 0 then
		local old_dim = getElementDimension(plr)
		setElementDimension(plr, math.random(1000,2000))
		setElementFrozen(plr, true)
		is_afk[plr] = old_dim
		exports.topbar:dm(getPlayerName(plr).." agora esta ausente (AFK)", root, 255, 100, 0)
		bindKey(plr, "W", "down", "afk")
		setPlayerName(plr, getPlayerName(plr).."_AFK")
		outputChatBox("A tag _AFK foi adicionada ao seu nome, lembre-se de removê-lo.", plr, 200, 200, 200)
		outputServerLog(getPlayerName(plr).." is now AFK")
	elseif is_afk[plr] then
		setElementDimension(plr, is_afk[plr])
		setElementFrozen(plr, false)
		is_afk[plr] = nil
		exports.topbar:dm(getPlayerName(plr).." agora está de volta ao jogo", root, 255, 100, 0)
		unbindKey(plr, "W", "down", "afk")
		setPlayerName(plr, string.gsub(getPlayerName(plr), "_AFK", ""))
		outputServerLog(getPlayerName(plr).." is now back in game")
	elseif getPlayerWantedLevel(plr) > 0 then
		exports.topbar:dm("AFK: Você não pode ficar ao AFK enquanto querido!", plr, 255, 0, 0)
	end
end
addCommandHandler("afk", go_afk)
addCommandHandler("ocupado", go_afk)
addCommandHandler("ausente", go_afk)
addCommandHandler("off", go_afk)
