--[[
********************************************************************************
	Project owner:		RageQuit community
	Project name: 		TopCity-RP
	Developers:   		Mr_Moose

	Source code:		https://github.com/404rq/TopCity-RP/
	Bugtracker: 		https://discuss.404rq.com/t/issues
	Suggestions:		https://discuss.404rq.com/t/development

	Version:    		Open source
	License:    		BSD 2-Clause
	Status:     		Stable release
********************************************************************************
]]--

-- Database connection setup, MySQL or fallback SQLite
local mysql_host        = exports.core:getMySQLHost() or nil
local mysql_database    = exports.core:getMySQLDatabase() or nil
local mysql_user        = exports.core:getMySQLUser() or nil
local mysql_pass        = exports.core:getMySQLPass() or nil
veh_data = dbConnect("mysql", "dbname="..mysql_database..";host="..mysql_host, mysql_user, mysql_pass, "autoreconnect=1")
if not veh_data then veh_data = dbConnect("sqlite", "veh.db") end

inventory_markers_veh = {}
inventory_markers = {}
vehicle_owners = {}
vehicles = {}
veh_blips = {}
veh_id_num = {}
veh_save_timers = {}
cooldown_vehshop_enter = {}
is_demo_ex = {}

--[[ Save the new bought vehcile to the database ]]--
function vehicleBuyRequest( model )
   	if model and not isGuestAccount( getPlayerAccount( client )) then
   		local price = 100
   		for x=1, #car_data do
   			for y=1, #car_data[x] do
   				if car_data[x][y][1] == model then
   					price = car_data[x][y][3]
   					break
   				end
   			end
   		end
   		price = price*priceMultiplier
		if model and price and getPlayerMoney(client) >= price then
			takePlayerMoney( client, price )
			exports.topbar:dm( "You have bought a "..getVehicleNameFromModel( model ), client, 0, 255, 0 )

			-- Save new vehicles to database
			dbExec(veh_data, "INSERT INTO vehicles VALUES (NULL,?,?,?,?,?,?,?,?,?,?,?,?,?)",
				getAccountName(getPlayerAccount( client )), model, 0, 0, 100, 100, 3, "TopCity-RP4", toJSON({0,0,0, 0,0,0}),
				toJSON({200,200,200, 200,200,200, 0,0,0, 0,0,0}), toJSON({}), toJSON({}), toJSON({}))
		elseif getPlayerMoney(client) < price then
			exports.topbar:dm( "Você não pode pagar esse veículo seu pequeno idiota!", client, 255, 0, 0 )
		end
	else
	   	exports.topbar:dm( "Você deve estar logado para comprar um veículo!", client, 255, 0, 0 )
	end
end
addEvent( "vehicleshop.onPlayerVehicleBuyRequest", true )
addEventHandler( "vehicleshop.onPlayerVehicleBuyRequest", root, vehicleBuyRequest )

--[[ Create a database table to store vehicle data ]]--
addEventHandler("onResourceStart", getResourceRootElement(),
function()
	dbExec(veh_data, "CREATE TABLE IF NOT EXISTS vehicles (ID INTEGER PRIMARY KEY, owner TEXT, model NUMERIC, "..
		"locked NUMERIC, engine NUMERIC, health NUMERIC, fuel NUMERIC, paint NUMERIC, platetext TEXT, pos TEXT, color TEXT, upgrades TEXT, inventory TEXT, headlight TEXT)")
end)

--[[ Loads all vehicles for a specific player, requires that the player is logged in ]]--
function loadMyVehicles(query)
	local result = dbPoll( query, 0 )
	if result then
    	for _, row in ipairs( result ) do
            addVehicle(row["ID"], row["owner"], row["model"], row["locked"], row["engine"],
            	row["health"], row["fuel"], row["paint"], row["platetext"], row["pos"], row["color"],
            	row["upgrades"], row["inventory"], row["headlight"])
    	end
	end
end
function getMyVehicles(veh_id)
	if getPlayerAccount( client ) and not isGuestAccount( getPlayerAccount( client )) and veh_id and
		getElementInterior(client) == 0 and getElementDimension(client) == 0 then
		dbQuery(loadMyVehicles, veh_data, "SELECT * FROM vehicles WHERE owner=? AND ID=?", getAccountName(getPlayerAccount( client )), tonumber(veh_id))
	elseif getElementInterior(client) ~= 0 or getElementDimension(client) ~= 0 then
		exports.topbar:dm( "Você não pode usar veículos aqui!", client, 255, 0, 0 )
	elseif not veh_id then
		exports.topbar:dm( "Por favor, especifique o ID do veículo", client, 255, 0, 0 )
	else
		exports.topbar:dm( "Você precisa estar logado para possuir e usar seus veículos!", client, 255, 0, 0 )
	end
end
addEvent( "vehicleshop.onShowVehicles", true )
addEventHandler( "vehicleshop.onShowVehicles", root, getMyVehicles )

--[[ Loads all vehicles for a specific player, requires that the player is logged in ]]--
function unloadMyVehicles(query)
	local result = dbPoll( query, 0 )
	if result then
    	for _, row in ipairs( result ) do
            saveAndRemoveVehicle(vehicles[row["ID"]],true)
    	end
	end
end
function hideMyVehicles(veh_id)
	if getPlayerAccount( client ) and not isGuestAccount( getPlayerAccount( client )) and veh_id then
		dbQuery(unloadMyVehicles, veh_data, "SELECT * FROM vehicles WHERE owner=? AND ID=?", getAccountName(getPlayerAccount( client )), tonumber(veh_id))
	elseif not veh_id then
		exports.topbar:dm( "Por favor, especifique o ID do veículo", client, 255, 0, 0 )
	else
		exports.topbar:dm( "Você precisa estar logado para possuir e usar seus veículos!", client, 255, 0, 0 )
	end
end
addEvent( "vehicleshop.onHideVehicles", true )
addEventHandler( "vehicleshop.onHideVehicles", root, hideMyVehicles )

--[[ Loads all vehicles for a specific player, requires that the player is logged in ]]--
function listAllMyVehicles(query)
	local result = dbPoll( query, 0 )
	if not result then return end

	local vehicle_data_to_client = {{ }}
	local plr = nil
    	for index, row in ipairs(result) do
    		-- Get all relevant data for the vehicle
    		vehicle_data_to_client[index] = { }
    		vehicle_data_to_client[index][1] = tonumber(row["ID"])
    		vehicle_data_to_client[index][2] = tonumber(row["model"])
    		vehicle_data_to_client[index][3] = tonumber(row["health"])
    		vehicle_data_to_client[index][4] = tonumber(row["fuel"])
    		vehicle_data_to_client[index][5] = tonumber(row["locked"])
    		vehicle_data_to_client[index][6] = tonumber(row["engine"])
    		vehicle_data_to_client[index][7] = row["pos"]
    		plr = getAccountPlayer(getAccount(row["owner"]))
    	end

    	-- Send data to client
    	if plr and isElement(plr) and getElementType(plr) then
    		triggerClientEvent( plr, "vehicleshop.onReceivePlayerVehicleData", plr, vehicle_data_to_client )
	end
end
function listMyVehicles( )
	if getPlayerAccount( client ) and not isGuestAccount( getPlayerAccount( client )) then
		dbQuery(listAllMyVehicles, veh_data, "SELECT * FROM vehicles WHERE owner=?", getAccountName(getPlayerAccount( client )))
	else
		exports.topbar:dm( "Você precisa estar logado para possuir e usar seus veículos!", client, 255, 0, 0 )
	end
end
addEvent( "vehicleshop.onListVehicles", true )
addEventHandler( "vehicleshop.onListVehicles", root, listMyVehicles )

--[[ Create a vehicle based on data from the vehicle database ]]--
function addVehicle(ID, owner, model, lock, engine, health, fuel, paint, platetext, pos, color, upgrades, inventory, hlight)
	if not getAccount( owner ) or not getAccountPlayer( getAccount( owner )) or getElementData(
		getAccountPlayer( getAccount( owner )), "Jailed") == "Yes" then return end
	if not vehicles[ID] then
		local x,y,z, rx,ry,rz = unpack( fromJSON( pos ))
		local isFirstSpawn = false
		if x == 0 and y == 0 and z == 0 then
			x,y,z = getElementPosition( getAccountPlayer( getAccount( owner )))
			rx,ry,rz = getElementRotation( getAccountPlayer( getAccount( owner )))
			z = z + 3
			isFirstSpawn = true
		end
		local veh = createVehicle( tonumber( model ), x,y,z )

                -- Reduce vehicle top speed and acceleration
                local bicycle_list = {[509]=true,[481]=true,[510]=true,[462]=true}
                local bike_list = {[581]=true,[509]=true,[481]=true,[462]=true,[521]=true,[463]=true,[510]=true,[522]=true,[461]=true,[448]=true,[468]=true,[586]=true}
                if getVehicleType(veh) == "Automobile" or bicycle_list[vehID] then
                        local result = getVehicleHandling(veh)
						local realism_index = 1.4
                        setVehicleHandling(vehicles[client], "engineAcceleration", tonumber(result["engineAcceleration"])/realism_index, false)
                        setVehicleHandling(vehicles[client], "engineInertia", tonumber(result["engineInertia"])*realism_index, false)
                        setVehicleHandling(vehicles[client], "brakeDeceleration", tonumber(result["brakeDeceleration"])/realism_index, false)
                        setVehicleHandling(vehicles[client], "brakeBias", tonumber(result["brakeBias"])/realism_index, false)
                        setVehicleHandling(vehicles[client], "percentSubmerged", tonumber(result["percentSubmerged"])*realism_index, false)

                        --Reduce max speed on bicycles and faggio
                        if bicycle_list[vehID] then
                                setVehicleHandling(vehicles[client], "maxVelocity", 40, false)
                        end
                end

		if supported_cars[getElementModel(veh)] then
			local dist = supported_cars[getElementModel(veh)]
			inventory_markers[veh] = createMarker(0, 0, -100, "cylinder", 3, 0, 0, 0, 0 )
			inventory_markers_veh[inventory_markers[veh]] = veh
			attachElements(inventory_markers[veh],veh,0,supported_cars[getElementModel(veh)],-1)
			addEventHandler( "onMarkerHit", inventory_markers[veh],
			function(hitElement,matchingDimension)
				if hitElement and isElement(hitElement) and getElementType(hitElement) == "player" and
					not getPedOccupiedVehicle(hitElement) and not getElementData(inventory_markers_veh[source],
					"vehicleshop.the_near_player_trunk") then
					exports.topbar:dm( "Veículo: Pressione F9 para abrir o inventário do veículo", hitElement, 0, 255, 0 )
					setElementData(hitElement,"vehicleshop.the_near_veh_trunk",inventory_markers_veh[source])
					setElementData(inventory_markers_veh[source],"vehicleshop.the_near_player_trunk",hitElement)
				elseif getElementData(inventory_markers_veh[source], "vehicleshop.the_near_player_trunk") then
					local name = getPlayerName(getElementData(inventory_markers_veh[source], "vehicleshop.the_near_player_trunk"))
					exports.topbar:dm( "Veículo: "..name.." está olhando o porta-malas deste veículo, aguarde", hitElement, 255, 100, 0 )
				end
			end)
			addEventHandler( "onMarkerLeave", inventory_markers[veh],
			function(leaveElement,matchingDimension)
				if leaveElement and isElement(leaveElement) and getElementType(leaveElement) == "player" then
					setElementData(leaveElement,"vehicleshop.the_near_veh_trunk",nil)
					setElementData(inventory_markers_veh[source],"vehicleshop.the_near_player_trunk",nil)
				end
			end)
		end
		if isFirstSpawn then
			warpPedIntoVehicle( getAccountPlayer( getAccount( owner )), veh )
		end
		veh_blips[veh] = createBlipAttachedTo(veh, 0, 2, 100, 100, 100, 200, 10, 9999, getAccountPlayer( getAccount( owner )))
		setElementRotation( veh, rx, ry, rz )
		vehicle_owners[veh] = owner
		veh_id_num[veh] = ID
		vehicles[ID] = veh
		local ar,ag,ab, br,bg,bb, cr,cg,cb, dr,dg,db = unpack( fromJSON( color ))
		local locked = false
		if lock == 1 then
			locked = true
		end
		if hlight then
			local hr,hg,hb = unpack( fromJSON( hlight ))
			if hr and hg and hb then
				setVehicleHeadLightColor( veh, hr, hg, hb )
			end
		end
		setVehicleColor( veh, ar,ag,ab, br,bg,bb, cr,cg,cb, dr,dg,db )
		setVehiclePaintjob( veh, tonumber( paint ))
		setVehicleLocked( veh, locked )
		setElementData( veh, "vehicleFuel", tonumber(fuel))
		health = tonumber(health*10)
		if health < 100 then health = 100 end
		setElementHealth( veh, health )
                setVehiclePlateText( veh, platetext )
		setElementData( veh, "owner", owner )
		setElementData( veh, "isOwnedVehicle", tonumber(ID))
		if getElementHealth( veh ) < 100 then
			setElementHealth( veh, 100 )
		end
		--outputChatBox(upgrades, getAccountPlayer( getAccount( owner )))
		for k, i in pairs( fromJSON( upgrades )) do
			addVehicleUpgrade( veh, i )
			--outputChatBox(i, getAccountPlayer( getAccount( owner )))
		end
	end
end

--[[ Manage saving of vehicle data ]]--
function saveVehicleData( thePlayer, seat, jacked )
	if vehicle_owners[source] then
		if isTimer(veh_save_timers[thePlayer]) then
			killTimer(veh_save_timers[thePlayer])
		end
   		veh_save_timers[thePlayer] = setTimer(saveVehicle, 5000, 0, source )
   	end
end
addEventHandler( "onVehicleEnter", getRootElement(), saveVehicleData )
function vehicleExit( thePlayer, seat, jacked )
    if isTimer(veh_save_timers[thePlayer]) then
		killTimer(veh_save_timers[thePlayer])
	end
end
addEventHandler( "onVehicleExit", root, vehicleExit )
function saveVehicle(veh)
	if veh and isElement(veh) then
		saveAndRemoveVehicle(veh, false)
	end
end

--[[ Destroys and saves a vehicle into vehicle database ]]--
function saveAndRemoveVehicle(veh, removeVeh)
        if not veh or not isElement(veh) or getElementType(veh) ~= "vehicle" then return end
	-- Ensure that the vehicle is owned by a player
	if vehicle_owners[veh] then
		-- Get vehicle data
		local x,y,z = getElementPosition( veh )
		local ar,ag,ab, br,bg,bb, cr,cg,cb, dr,dg,db = getVehicleColor( veh, true )
		local rx,ry,rz = getElementRotation( veh )
		local fuel = getElementData( veh, "vehicleFuel" )
		local paint = getVehiclePaintjob( veh )
                local platetext = getVehiclePlateText( veh )
		local health = tostring(math.floor(tonumber(getElementHealth( veh ))/10))
		local locked = 0
		if isVehicleLocked( veh ) then
			locked = 1
		end
		local engine = 0
		if getVehicleEngineState( veh ) then
			engine = 1
		end
		local ID = getElementData( veh, "isOwnedVehicle" )
		if ID then
			-- Save to database
			dbExec(veh_data, "UPDATE vehicles SET owner=?, locked=?, engine=?, health=?, fuel=?, paint=?, platetext=?, pos=?, color=?, upgrades=? WHERE ID=?",
				vehicle_owners[veh], locked, engine, health, fuel, paint, platetext, toJSON({x,y,z, rx,ry,rz}),
				toJSON({ar,ag,ab, br,bg,bb, cr,cg,cb, dr,dg,db}), toJSON(getVehicleUpgrades( veh )), ID)

			-- Clean up and free memory
			if removeVeh then
				-- Remove inventory marker
				if inventory_markers_veh[inventory_markers[veh]] and isElement(inventory_markers_veh[inventory_markers[veh]]) then
					inventory_markers_veh[inventory_markers[veh]] = nil
				end
				if inventory_markers[veh] and isElement(inventory_markers[veh]) then
					destroyElement(inventory_markers[veh])
				end
				vehicles[ID] = nil
				vehicle_owners[veh] = nil
				destroyElement(veh_blips[veh])
				destroyElement(veh)
			end
		end
	elseif isElement(veh) then
		destroyElement(veh)
	end
end
addEvent( "vehicleshop.onPlayerVehicleDestroy", true )
addEventHandler ( "vehicleshop.onPlayerVehicleDestroy", root, saveAndRemoveVehicle )

--[[ Lock vehicle from client ]]--
function lockVehicle(veh_id, lock_id)
	if getPlayerAccount( client ) and not isGuestAccount( getPlayerAccount( client )) and
		veh_id and vehicles[veh_id] and isElement(vehicles[veh_id]) then
		if isVehicleLocked( vehicles[veh_id] ) then
			setVehicleLocked( vehicles[veh_id], false )
			-- Save to database
			dbExec(veh_data, "UPDATE vehicles SET locked=? WHERE ID=?", 0, veh_id)
			exports.topbar:dm( "Seu veículo foi desbloqueado!", client, 0, 255, 0 )
		else
			setVehicleLocked( vehicles[veh_id], true )
			-- Save to database
			dbExec(veh_data, "UPDATE vehicles SET locked=? WHERE ID=?", 1, veh_id)
			exports.topbar:dm( "Seu veículo foi trancado!", client, 0, 255, 0 )
		end
	elseif getPlayerAccount( client ) and not isGuestAccount( getPlayerAccount( client )) and veh_id and lock_id then
		-- Save to database
		dbExec(veh_data, "UPDATE vehicles SET locked=? WHERE ID=?", lock_id, veh_id)
	else
		exports.topbar:dm( "Você precisa estar logado para possuir e usar seus veículos!", client, 255, 0, 0 )
	end
end
addEvent( "vehicleshop.onLockVehicle", true )
addEventHandler( "vehicleshop.onLockVehicle", root, lockVehicle )

--[[ Toggle vehicle engine state from client ]]--
function toggleVehicleEngine(veh_id, engine_id)
	if getPlayerAccount( client ) and not isGuestAccount( getPlayerAccount( client )) and
		veh_id and vehicles[veh_id] and isElement(vehicles[veh_id]) then
		if getVehicleEngineState( vehicles[veh_id] ) then
			setVehicleEngineState( vehicles[veh_id], false )
			-- Save to database
			dbExec(veh_data, "UPDATE vehicles SET engine=? WHERE ID=?", 0, veh_id)
			exports.topbar:dm( "O motor do veículo foi desligado!", client, 0, 255, 0 )
		else
			setVehicleEngineState( vehicles[veh_id], true )
			-- Save to database
			dbExec(veh_data, "UPDATE vehicles SET engine=? WHERE ID=?", 1, veh_id)
			exports.topbar:dm( "O motor do veículo foi ligado!", client, 0, 255, 0 )
		end
	elseif getPlayerAccount( client ) and not isGuestAccount( getPlayerAccount( client )) and veh_id and engine_id then
		-- Save to database
		dbExec(veh_data, "UPDATE vehicles SET engine=? WHERE ID=?", engine_id, veh_id)
	else
		exports.topbar:dm( "Você precisa estar logado para possuir e usar seus veículos!", client, 255, 0, 0 )
	end
end
addEvent( "vehicleshop.onVehicleEngineToggle", true )
addEventHandler( "vehicleshop.onVehicleEngineToggle", root, toggleVehicleEngine )

--[[ Sell a vehicle from client GUI ]]--
function returnWeaponsOnSell(query)
	local result = dbPoll( query, 0 )
	if not result then return end
	local items = nil
	local plr = nil
	local veh_id = nil

	-- Get the json string
        for _,row in ipairs( result ) do
                -- Get all relevant data for the vehicle
                items = row["inventory"]
                plr = getAccountPlayer(getAccount(row["owner"]))
    	        veh_id = row["ID"]
    	        break
        end

        -- Extract data and give weapons back to the owner
        local data_table = fromJSON(items or "") or { }
	for k, v in pairs(data_table) do
		giveWeapon(plr, getWeaponIDFromName(k), tonumber(v))
		outputChatBox(k.." foi restaurado com sucesso ("..tostring(v)..") balas", plr, 255, 255, 255)
	end

        -- Send data to client
        if player then
    	           triggerClientEvent( player, "vehicleshop.onReceiveInventoryItems", player, vehicle_data_to_client )
	end

	-- Remove vehicle from database
	dbExec(veh_data, "DELETE FROM vehicles WHERE ID=?", veh_id)
end
function sellVehicle(veh_id, model)
	if getPlayerAccount( client ) and not isGuestAccount( getPlayerAccount( client )) and veh_id and model then
		-- Restore weapons to it's owner
		dbQuery(returnWeaponsOnSell, veh_data, "SELECT inventory, owner, ID FROM vehicles WHERE owner=? AND ID=?",
			getAccountName(getPlayerAccount( client )), tonumber(veh_id))

		-- Return money
		local price = 0
		for i=1, #car_data do
			if car_data[i] then
				for key, value in pairs(car_data[i]) do
					if value[1] and value[1] == model then
						price = value[3]
					end
				end
			end
		end

		-- Clean up if vehicle isn't hidden while selling
		if isElement(vehicles[veh_id]) then
			local veh = vehicles[veh_id]
			if inventory_markers_veh[inventory_markers[veh]] and isElement(inventory_markers_veh[inventory_markers[veh]]) then
				inventory_markers_veh[inventory_markers[veh]] = nil
			end
			if inventory_markers[veh] and isElement(inventory_markers[veh]) then
				destroyElement(inventory_markers[veh])
			end
			vehicle_owners[veh] = nil
			destroyElement(veh_blips[veh])
			destroyElement(veh)
			vehicles[veh_id] = nil
		end

		givePlayerMoney(client, math.floor(price*priceMultiplier*0.8))
		exports.topbar:dm( "Seu veículo foi vendido por 80% do preço", client, 0, 255, 0 )
	else
		exports.topbar:dm( "Você precisa estar logado para possuir e usar seus veículos!", client, 255, 0, 0 )
	end
end
addEvent( "vehicleshop.onVehicleSell", true )
addEventHandler( "vehicleshop.onVehicleSell", root, sellVehicle )

--[[ Respawn a broken vehicle ]]--
function respawnVehicleToStart(veh_id)
	if getPlayerAccount( client ) and not isGuestAccount( getPlayerAccount( client )) and
		veh_id and not vehicles[veh_id] and not isElement(vehicles[veh_id]) then
                local dist_to_cop = exports.police:distanceToCop(client)
                -- Tolerate less than 30 seconds violent time or distance to cop larger than 180m
		if ((tonumber(getElementData(client, "violent_seconds")) or 0) < 50 or
                        dist_to_cop > 180) and getElementData(client, "Jailed") ~= "Yes" then
			local price = 100
			if price and getPlayerMoney(client) > price then
				takePlayerMoney( client, price )
				-- Save to database
				dbExec(veh_data, "UPDATE vehicles SET pos=? WHERE ID=?", toJSON({ 0,0,0, 0,0,0 }), veh_id)
				exports.topbar:dm( "Seu veículo foi respawned!", client, 0, 255, 0 )
			else
				exports.topbar:dm( "Você não pode recuperar seu veículo, você precisa de R$500!", client, 255, 0, 0 )
			end
		elseif getElementData(client, "Jailed") == "Yes" then
			exports.topbar:dm( "Você não pode recuperar enquanto estiver preso!", client, 255, 0, 0 )
		elseif dist_to_cop <= 180 then
			exports.topbar:dm( "Você é violento ou um policial!", client, 255, 0, 0 )
		end
	elseif vehicles[veh_id] and isElement(vehicles[veh_id]) then
		exports.topbar:dm( "Você deve esconder o seu veículo antes de poder recuperá-lo!", client, 255, 0, 0 )
	elseif not getPlayerAccount( client ) or isGuestAccount( getPlayerAccount( client )) then
		exports.topbar:dm( "Você precisa estar logado para possuir e usar seus veículos!", client, 255, 0, 0 )
	end
end
addEvent( "vehicleshop.onVehicleRespawn", true )
addEventHandler( "vehicleshop.onVehicleRespawn", root, respawnVehicleToStart )

--[[ Withdraw weapons to vehicle inventory ]]--
temp_weapon_store = {}
temp_ammo_store = {}
temp_plr_store = {}
function onVehicleWeaponWithdrawGet(query)
	local result = dbPoll( query, 0 )
	if not result then return end
    for _, row in ipairs( result ) do
    	-- Add weapon to JSON string (Only executed once)
    	local input_table = fromJSON(row["inventory"])
    	local plr_owner = temp_plr_store[row["ID"]]
    	if not plr_owner or not input_table then break end
    	-- Debug info
    	--outputChatBox(row["inventory"],plr_owner)

    	-- Update value to be saved into database
    	input_table[temp_weapon_store[plr_owner]] = ((input_table[
    		temp_weapon_store[plr_owner]] or 0) + temp_ammo_store[plr_owner])
    	local new_res = toJSON(input_table)

    	-- Debug info
    	--outputChatBox(new_res, plr_owner)

    	-- Cleanup
    	temp_weapon_store[plr_owner] = nil
    	temp_ammo_store[plr_owner] = nil
    	temp_plr_store[row["ID"]] = nil

    	-- Save to database
		dbExec(veh_data, "UPDATE vehicles SET inventory=? WHERE ID=?", new_res, row["ID"])
		break
	end
end
function onVehicleWeaponWithdraw(veh_id, weap, ammo)
	if getPlayerAccount( client ) and not isGuestAccount( getPlayerAccount( client )) and veh_id and weap and ammo then
		takeWeapon(client, getWeaponIDFromName(weap), tonumber(ammo))

		-- Save to temp storage
		temp_weapon_store[client] = weap
		temp_ammo_store[client] = ammo
		temp_plr_store[veh_id] = client

		-- Save to database
		dbQuery(onVehicleWeaponWithdrawGet, veh_data, "SELECT inventory, owner, ID FROM vehicles WHERE ID=?", tonumber(veh_id))

		exports.topbar:dm( "Sua arma foi retirada", client, 0, 255, 0 )
	elseif not getPlayerAccount( client ) or isGuestAccount( getPlayerAccount( client )) then
		exports.topbar:dm( "Você precisa estar logado para possuir e usar seus veículos!", client, 255, 0, 0 )
	else
		exports.topbar:dm( "Erro, por favor especifique uma arma para colocar no porta-malas do seu veículo!", client, 255, 0, 0 )
	end
end
addEvent( "vehicleshop.onVehicleWeaponWithdraw", true )
addEventHandler( "vehicleshop.onVehicleWeaponWithdraw", root, onVehicleWeaponWithdraw )

--[[ Deposit from vehicle inventory ]]--
function onVehicleWeaponDepositGet(query)
	local result = dbPoll( query, 0 )
	if not result then return end
    for _, row in ipairs( result ) do
    	-- Add weapon to JSON string (Only executed once)
    	local input_table = fromJSON(row["inventory"])
    	local plr_owner = temp_plr_store[row["ID"]]
    	if input_table and plr_owner and temp_weapon_store[plr_owner] then
    		local new_val = (input_table[temp_weapon_store[plr_owner]] or 0) - temp_ammo_store[plr_owner]

	    	-- Debug info
	    	--outputChatBox(row["inventory"], plr_owner)

	    	-- Update value to be saved into database
	    	if new_val > 0 then
	    		input_table[temp_weapon_store[plr_owner]] = new_val
	    	else
	    		input_table[temp_weapon_store[plr_owner]] = nil
	    	end
	    	local new_res = toJSON(input_table)

	    	-- Debug info
	    	--outputChatBox(new_res, plr_owner)

	    	-- Cleanup
	    	temp_weapon_store[plr_owner] = nil
	    	temp_ammo_store[plr_owner] = nil
	    	temp_plr_store[row["ID"]] = nil

	    	-- Save to database
			dbExec(veh_data, "UPDATE vehicles SET inventory=? WHERE ID=?", new_res, row["ID"])
		end
		break
	end
end
function onVehicleWeaponDeposit(veh_id, weap, ammo)
	if getPlayerAccount( client ) and not isGuestAccount( getPlayerAccount( client )) and veh_id and weap and ammo then
		giveWeapon(client, getWeaponIDFromName(weap), tonumber(ammo))

		-- Save to temp storage
		temp_weapon_store[client] = weap
		temp_ammo_store[client] = ammo
		temp_plr_store[veh_id] = client

		-- Save to database
		dbQuery(onVehicleWeaponDepositGet, veh_data, "SELECT inventory, owner, ID FROM vehicles WHERE ID=?", tonumber(veh_id))
		exports.topbar:dm( "Sua arma foi depositada", client, 0, 255, 0 )
	elseif not getPlayerAccount( client ) or isGuestAccount( getPlayerAccount( client )) then
		exports.topbar:dm( "Você precisa estar logado para possuir e usar seus veículos!", client, 255, 0, 0 )
	else
		exports.topbar:dm( "Erro, por favor especifique uma arma para colocar no porta-malas do seu veículo!", client, 255, 0, 0 )
	end
end
addEvent( "vehicleshop.onVehicleWeaponDeposit", true )
addEventHandler( "vehicleshop.onVehicleWeaponDeposit", root, onVehicleWeaponDeposit )

--[[ Get weapons from inventory ]]--
function getInventoryWeapons(query)
	local result = dbPoll( query, 0 )
	if result then
		local vehicle_data_to_client = nil
		local plr = nil
    	for _,row in ipairs( result ) do
    		-- Get all relevant data for the vehicle
    		vehicle_data_to_client = row["inventory"]
    		plr = temp_plr_store[row["ID"]]

    		-- Send data to client
	    	if plr then
	    		triggerClientEvent( plr, "vehicleshop.onReceiveInventoryItems", plr, vehicle_data_to_client )
			end

			-- Cleanup
			temp_plr_store[row["ID"]] = nil
			break
		end
	end
end
function openInventory(veh_id)
	if getPlayerAccount( client ) and not isGuestAccount( getPlayerAccount( client )) and veh_id then
		temp_plr_store[veh_id] = client
		setVehicleDoorOpenRatio( getElementData(client, "vehicleshop.the_near_veh_trunk"), 1, 1, 1000 )
		dbQuery(getInventoryWeapons, veh_data, "SELECT inventory, owner, ID FROM vehicles WHERE ID=?", tonumber(veh_id))
	else
		exports.topbar:dm( "Você precisa estar logado para possuir e usar seus veículos!", client, 255, 0, 0 )
	end
end
addEvent( "vehicleshop.onOpenInventory", true )
addEventHandler( "vehicleshop.onOpenInventory", root, openInventory )
function closeInventory()
	setVehicleDoorOpenRatio( getElementData(client, "vehicleshop.the_near_veh_trunk"), 1, 0, 1000 )
end
addEvent( "vehicleshop.onCloseInventory", true )
addEventHandler( "vehicleshop.onCloseInventory", root, closeInventory )

--[[ Toggle vehicle engine state from client ]]--
function vehicleHeadLightColors(player, cmd, r,g,b)
    if getPlayerAccount( player ) and not isGuestAccount( getPlayerAccount( player )) and
        r and g and b and veh_id_num[getPedOccupiedVehicle(player)] then
       	-- Save to database and update colors
        dbExec(veh_data, "UPDATE vehicles SET headlight=? WHERE ID=?", toJSON({r,g,b}), veh_id_num[getPedOccupiedVehicle(player)])
        setVehicleHeadLightColor( getPedOccupiedVehicle(player), r, g, b )
    else
        exports.topbar:dm( "Você precisa estar logado para possuir e usar seus veículos!", client, 255, 0, 0 )
    end
end
addCommandHandler("hlcolor", vehicleHeadLightColors)
addCommandHandler("headlight", vehicleHeadLightColors)
addCommandHandler("headlightcol", vehicleHeadLightColors)
addCommandHandler("headlightcolor", vehicleHeadLightColors)
addCommandHandler("setheadlight", vehicleHeadLightColors)

addCommandHandler("gtainfo", function(plr, cmd)
	outputChatBox("[TopCity-RP] "..getResourceName(
	getThisResource())..", by: "..getResourceInfo(
        getThisResource(), "author")..", v-"..getResourceInfo(
        getThisResource(), "version")..", is represented", plr)
end)

function saveAllVehicles(quitType)
	dbQuery(unloadMyVehicles, veh_data, "SELECT * FROM vehicles WHERE owner=?", getAccountName(getPlayerAccount( source )))
end
addEventHandler("onPlayerQuit", root, saveAllVehicles)
