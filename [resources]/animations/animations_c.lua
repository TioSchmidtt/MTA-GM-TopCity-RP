
--[[ Initialize the GUI ]]--
local resX, resY = guiGetScreenSize()
function create_gui()
	win_anim = exports.UI:createWindow(resX-400, resY/2-250, 400, 500, "Animações", false)
	guiSetVisible(win_anim, false)
	category_list = guiCreateGridList(10, 33, 187, 400, false, win_anim)
	anim_list = guiCreateGridList(203,33,187,400,false,win_anim)
	column1 = guiGridListAddColumn(category_list,"Categoria",0.8)
	column2 = guiGridListAddColumn(anim_list,"Animação",0.8)
	btn_stop = guiCreateButton(10,439,187,50,"Parar Animação",false,win_anim)
	btn_close = guiCreateButton(203,439,187,50,"Fechar",false,win_anim)
	addEventHandler("onClientGUIClick",btn_stop,function()
		triggerServerEvent("animations.setAnimation",localPlayer,nil,nil)
		setElementFrozen( localPlayer, false )
	end)
	for k,v in pairs(animations_table) do
		local row = guiGridListAddRow(category_list)
		guiGridListSetItemText(category_list, row, column1, k, false, false)
	end
	addEventHandler("onClientGUIClick", category_list, load_anim)
	addEventHandler("onClientGUIClick", anim_list, set_anim)
	addEventHandler("onClientGUIClick", btn_close, window_close)
end
addEventHandler("onClientResourceStart", resourceRoot, create_gui)

--[[ Close the window ]]--
function window_close()
	guiSetVisible(win_anim, false)
	exports.UI:showGUICursor(false)
end

--[[ Toggle window visibility ]]--
function window_toggle()
	if not guiGetVisible(win_anim) then
		guiSetVisible(win_anim, true)
		exports.UI:showGUICursor(true)
	else
		guiSetVisible(win_anim, false)
		exports.UI:showGUICursor(false)
	end
end
bindKey("F10", "down", window_toggle)

--[[ Load animations from category ]]--
function load_anim()
	curr_category = guiGridListGetItemText(category_list, guiGridListGetSelectedItem(category_list), 1)
	if curr_category ~= "" then
		guiGridListClear(anim_list)
		for k,v in ipairs(animations_table[curr_category]) do
			local row = guiGridListAddRow(anim_list)
			guiGridListSetItemText(anim_list, row, column1, v[1], false, false)
		end
	end
	if curr_category == "" then
		guiGridListClear(anim_list)
	end
end

--[[ Apply animation ]]--
function set_anim()
	local curr_anim = guiGridListGetItemText(anim_list,guiGridListGetSelectedItem(anim_list),1)
	if curr_anim == "" then return end

	-- Skip if dead
	if isPedDead(localPlayer) then
		exports.topbar:dm("Você não pode usar animações enquanto está morto.", 255, 0, 0)
		return
	end

	-- Skip if inside a vehicle
	if isPedInVehicle(localPlayer) then
		exports.topbar:dm("Você não pode usar animações enquanto estiver em um veículo.", 255, 0, 0)
		return
	end

	-- Start animation
	triggerServerEvent("animations.setAnimation", localPlayer, curr_category, curr_anim)
end
