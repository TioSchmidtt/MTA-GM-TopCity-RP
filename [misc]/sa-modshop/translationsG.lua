﻿selectedLanguage = "en"
--->https://dubmodsmta.blogspot.com.br/
--->https://dubmodsmta.blogspot.com.br/
availableTranslations = {
	["en"] = {
		["menu.mainMenu"] = "Categorias",
		["menu.performance"] = "Performance",
		["menu.optical"] = "Ótico",
		["menu.extras"] = "Extras",
		["menu.color"] = "Cores",
		
		["menu.performance.engine"] = "Motor",
		["menu.performance.turbo"] = "Turbo",
		["menu.performance.nitro"] = "Nitro",
		["menu.performance.tires"] = "Pneus",
		["menu.performance.brakes"] = "Freios",
		["menu.performance.weightReduction"] = "Redução de peso",
		
		["menu.optical.frontBumper"] = "Para-choque dianteiro",
		["menu.optical.rearBumper"] = "Para-choque traseiro",
		["menu.optical.hood"] = "Capô",
		["menu.optical.exhaust"] = "Exaustor",
		["menu.optical.spoiler"] = "Spoiler",
		["menu.optical.wheels"] = "Rodas",
		["menu.optical.sideSkirt"] = "Saias laterais",
		["menu.optical.roofScoop"] = "Teto",
		["menu.optical.hidraulics"] = "Hidraulica",
		["menu.optical.lampColor"] = "Cor do farol",
		
		["menu.extras.frontWheelSize"] = "Tamanho da roda dianteira",
		["menu.extras.rearWheelSize"] = "Tamanho da roda traseira",
		["menu.extras.offroad"] = "Offroad",
		["menu.extras.driveType"] = "Tipo de tração",
		["menu.extras.bulletproofTires"] = "Pneus a prova de balas",
		["menu.extras.lsdDoor"] = "LSD porta",
		["menu.extras.steeringLock"] = "Bloqueio da direção",
		["menu.extras.numberplate"] = "Placa",
		
		["tuningPack.0"] = "Padrão",
		["tuningPack.1"] = "Bronze Pack",
		["tuningPack.2"] = "Prata Pack",
		["tuningPack.3"] = "Ouro Pack",
		["tuningPack.wheelSize.veryNarrow"] = "Muito limitado",
		["tuningPack.wheelSize.narrow"] = "Limitado",
		["tuningPack.wheelSize.wide"] = "Largo",
		["tuningPack.wheelSize.veryWide"] = "Muito largo",
		["tuningPack.offroad.dirt"] = "Sujeira",
		["tuningPack.offroad.sand"] = "Areia",
		["tuningPack.driveType.front"] = "Tração dianteira",
		["tuningPack.driveType.all"] = "Tração total",
		["tuningPack.driveType.rear"] = "Tração traseira",
		["tuningPack.bulletproofTires"] = "À prova de balas",
		["tuningPack.numberplate.random"] = "Aleatório",
		["tuningPack.numberplate.custom"] = "Customizado",
		
		["tuningPack.optical.neon.1"] = "Branco",
		["tuningPack.optical.neon.2"] = "Azul",
		["tuningPack.optical.neon.3"] = "Verde",
		["tuningPack.optical.neon.4"] = "Vermelho",
		["tuningPack.optical.neon.5"] = "Amarelo",
		["tuningPack.optical.neon.6"] = "Rosa",
		["tuningPack.optical.neon.7"] = "Laranja",
		["tuningPack.optical.neon.8"] = "Azul claro",
		["tuningPack.optical.neon.9"] = "Rasta",
		["tuningPack.optical.neon.10"] = "Branco + azul claro",
		
		["tuningPrice.free"] = "Gratuito",
		["tuningPack.remove"] = "Remover",
		["tuningPack.install"] = "Instalar",
		["tuning.active"] = "Ativo",
		
		["navbar.select"] = "Selecionar",
		["navbar.buy"] = "Comprar",
		["navbar.navigate"] = "Navegar",
		["navbar.back"] = "Voltar",
		["navbar.exit"] = "Sair",
		["navbar.camera"] = "Mover camera",
		
		["notification.error.notCompatible"] = "O componente selecionado (%s) não é compatível com seu veículo!",
		["notification.error.itemIsPurchased"] = "O componente selecionado (%s) já está instalado no seu veículo!",
		["notification.error.notEnoughMoney"] = "Você não tem dinheiro suficiente!",
		["notification.success.purchased"] = "Sucesso na compra deste componente!",
		["notification.warning.airRideInstalled"] = "AVISO!\nSuspensão está instalado em seu veículo.\nPor favor, remova para adicionar hidraulica!",
		
		["prompt.text"] = "Você tem certeza que deseja comprar?",
		["prompt.info.1"] = "Elemento selecionado",
		["prompt.info.2"] = "Preço do tuning",
		["prompt.button.1"] = "Comprar (Enter)",
		["prompt.button.2"] = "Cancelar (Backspace)",
		
		["message.airride.error"] = "Uso: /airride [0 - 5] (0 => Suspensão padrao)",
	},
	["hu"] = {
		["menu.mainMenu"] = "Kategóriák",
		["menu.performance"] = "Teljesítmény",
		["menu.optical"] = "Optika",
		["menu.extras"] = "Extrák",
		["menu.color"] = "Festés",
		
		["menu.performance.engine"] = "Motor",
		["menu.performance.turbo"] = "Turbó",
		["menu.performance.nitro"] = "Nitró",
		["menu.performance.tires"] = "Gumik",
		["menu.performance.brakes"] = "Fékek",
		["menu.performance.weightReduction"] = "Súlycsökkentés",
		
		["menu.optical.frontBumper"] = "Első lökhárító",
		["menu.optical.rearBumper"] = "Hátsó lökhárító",
		["menu.optical.hood"] = "Motorháztető",
		["menu.optical.exhaust"] = "Kipufogó",
		["menu.optical.spoiler"] = "Légterelő",
		["menu.optical.wheels"] = "Kerekek",
		["menu.optical.sideSkirt"] = "Küszöb",
		["menu.optical.roofScoop"] = "Tetőlégterelő",
		["menu.optical.hidraulics"] = "Hidraulika",
		["menu.optical.lampColor"] = "Izzó szín",
		
		["menu.extras.frontWheelSize"] = "Első kerék szélessége",
		["menu.extras.rearWheelSize"] = "Hátsó kerék szélessége",
		["menu.extras.offroad"] = "Offroad optimalizáció",
		["menu.extras.driveType"] = "Meghajtás",
		["menu.extras.bulletproofTires"] = "Golyóálló kerekek",
		["menu.extras.lsdDoor"] = "LSD Ajtó",
		["menu.extras.steeringLock"] = "Fordulási szög",
		["menu.extras.numberplate"] = "Rendszám",
		
		["tuningPack.0"] = "Alapértelmezett",
		["tuningPack.1"] = "Bronz csomag",
		["tuningPack.2"] = "Ezüst csomag",
		["tuningPack.3"] = "Arany csomag",
		["tuningPack.wheelSize.veryNarrow"] = "Extra keskeny",
		["tuningPack.wheelSize.narrow"] = "Keskeny",
		["tuningPack.wheelSize.wide"] = "Széles",
		["tuningPack.wheelSize.veryWide"] = "Extra széles",
		["tuningPack.offroad.dirt"] = "Terep",
		["tuningPack.offroad.sand"] = "Murva",
		["tuningPack.driveType.front"] = "Elsőkerék meghajtás",
		["tuningPack.driveType.all"] = "Összkerék meghajtás",
		["tuningPack.driveType.rear"] = "Hátsókerék meghajtás",
		["tuningPack.bulletproofTires"] = "Golyóálló",
		["tuningPack.numberplate.random"] = "Véletlenszerű",
		["tuningPack.numberplate.custom"] = "Egyedi",
		
		["tuningPack.optical.neon.1"] = "Fehér",
		["tuningPack.optical.neon.2"] = "Kék",
		["tuningPack.optical.neon.3"] = "Zöld",
		["tuningPack.optical.neon.4"] = "Piros",
		["tuningPack.optical.neon.5"] = "Citromsárga",
		["tuningPack.optical.neon.6"] = "Rózsaszín",
		["tuningPack.optical.neon.7"] = "Narancssárga",
		["tuningPack.optical.neon.8"] = "Világoskék",
		["tuningPack.optical.neon.9"] = "Raszta",
		["tuningPack.optical.neon.10"] = "Fehér + Világoskék",
		
		["tuningPrice.free"] = "Ingyenes",
		["tuningPack.remove"] = "Eltávolítás",
		["tuningPack.install"] = "Beszerelés",
		["tuning.active"] = "Felszerelve",
		
		["navbar.select"] = "Kiválasztás",
		["navbar.buy"] = "Vásárlás",
		["navbar.navigate"] = "Navigáció",
		["navbar.back"] = "Visszalépés",
		["navbar.exit"] = "Kilépés",
		["navbar.camera"] = "Kamera mozgatás",
		
		["notification.error.notCompatible"] = "A kiválasztott elem (%s) nem kompatibilis a járműveddel!",
		["notification.error.itemIsPurchased"] = "A kiválasztott elem (%s) már megtalálható a járműveden!",
		["notification.error.notEnoughMoney"] = "Nincs elég pénzed a kiválasztott elem megvásárlásához!",
		["notification.success.purchased"] = "Sikeresen megvásároltad a kiválasztott elemet!",
		["notification.warning.airRideInstalled"] = "FIGYELMEZTETÉS!\nAz Air-Ride telepítve van a járművedbe.\nKérlek távolítsd el a Hidraulika telepítése előtt",
		
		["prompt.text"] = "Tényleg meg szeretnéd vásárolni a kiválasztott tuningot?",
		["prompt.info.1"] = "Kiválasztott tuning",
		["prompt.info.2"] = "A tuning ára",
		["prompt.button.1"] = "Vásárlás (Enter)",
		["prompt.button.2"] = "Mégsem (Backspace)",
		
		["message.airride.error"] = "Használat: /airride [0 - 5] (0 => Gyári hasmagasság)",
	},
}

function getLocalizedText(stringCode, ...)
	if stringCode then
		if availableTranslations[selectedLanguage] then
			if availableTranslations[selectedLanguage][stringCode] then
				if ... then
					return availableTranslations[selectedLanguage][stringCode]:format(...)
				else
					return availableTranslations[selectedLanguage][stringCode]
				end
			end
		end
	end
	
	if ... then
		return stringCode .. "|" .. table.concat({...}, "|")
	else
		return stringCode
	end
end