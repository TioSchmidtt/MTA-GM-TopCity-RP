--# Fix on all resolutions
local screenW,screenH = guiGetScreenSize()
local resW,resH = 1280,720
local sW,sH =  (screenW/resW), (screenH/resH)

local validWeaponSlots =
        {
        [ 2 ] = true,
        [ 3 ] = true,
        [ 4 ] = true,
        [ 5 ] = true,
        [ 6 ] = true,
        [ 7 ] = true,
        [ 8 ] = true,
		}

addEventHandler("onClientRender", root,
    function()
		--# Player
        local hour, mins = getTime ()
        local time = hour .. ":" .. (((mins < 10) and "0"..mins) or mins)
        local zone = getZoneName (getElementPosition(getLocalPlayer()))
		local money = convertNumber(getPlayerMoney(localPlayer))
		local health = math.floor (getElementHealth ( getLocalPlayer() ))
        local armor = math.floor(getPedArmor ( getLocalPlayer() ))
           -- dxDrawText(tostring (time), 1150*sW, 17*sH, 1270*sW, 38*sH, tocolor(255, 255, 255, 255), 1.25, "default-bold", "center", "center", false, false, false, false, false)
		   -- dxDrawText("VIDA: "..health.."% | COLETE: "..armor.."%", 1150*sW, 45*sH, 1270*sW, 45*sH, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, false, false, false)
			--dxDrawText("R$ "..money, 1150*sW, 50*sH, 1270*sW, 70*sH, tocolor(255, 255, 255, 255), 1.25, "default-bold", "left", "center", false, false, true, true, false)


		local oxygen = getPedOxygenLevel (getLocalPlayer())
            if  ( oxygen < 1000 or isElementInWater(getLocalPlayer()) ) then
	            dxDrawText("Oxygen: "..math.floor(oxygen/10).."%", 1150*sW, 100*sH, 1270*sW, 115*sH, tocolor(255, 255, 255, 255), 1.00, "default-bold", "left", "center", false, false, true, true, false)
            end
		if getPedControlState ("aim_weapon") or isPedDoingGangDriveby(getLocalPlayer()) then
		    local ammo = getPedTotalAmmo (getLocalPlayer())
            local clip = getPedAmmoInClip (getLocalPlayer())
	        local weaponID = getPedWeapon(localPlayer)
	        local weapName = getWeaponNameFromID(weaponID)
	        local weapSlot = getPedWeaponSlot(getLocalPlayer())
            local xBone,yBone,zBone = getPedBonePosition(getLocalPlayer(), 8)
            local zBone = zBone - 0.5
            local xSP,ySP = getScreenFromWorldPosition ( xBone,yBone,zBone )
                if ( xSP and ySP ) then
                    dxDrawText(weapName.." "..clip.." | "..ammo, xSP+100,ySP, 290*sW, 250*sH, tocolor (255, 255, 255, 255), 1.25*sW,1.25*sH,"default-bold","left","top",false,false,false,true)
		                if ( clip < 1 and validWeaponSlots [ weapSlot ]  ) then
                            dxDrawText("Reloading...", xSP+100,ySP+20, 290*sW, 250*sH, tocolor (255, 255, 255, 255), 1.25*sW,1.25*sH,"default-bold","left","top",false,false,false,true)
                        end
		        end
		end
        --# Vehicle
		local vehicle = getPedOccupiedVehicle(getLocalPlayer())
	        if ( vehicle ) then
		        local speedx, speedy, speedz = getElementVelocity ( vehicle  )
		        local actualspeed = (speedx^2 + speedy^2 + speedz^2)^(0.5)
		        local kmh = math.floor(actualspeed*180)
		            if getElementHealth(vehicle) >= 999 then
		                vehiclehealth = 100
		            else
		                vehiclehealth = math.floor(getElementHealth ( vehicle )/10)
		            end
			           -- dxDrawText(kmh.." km/h", 1160*sW, 653*sH, 1190*sW, 680*sH, tocolor(255, 255, 255, 255), 1.25, "default-bold", "left", "center", false, false, false, false, false)
                 -- dxDrawText("Damage: "..vehiclehealth.." %", 1150*sW, 680*sH, 1270*sW, 695*sH, tocolor(255, 255, 255, 255), 1.00, "default-bold", "left", "center", false, false, false, false, false)
	                end
            local vehicle = getPedOccupiedVehicle(getLocalPlayer())
	            if ( vehicle ) then
			        local nitro = getVehicleNitroLevel(getPedOccupiedVehicle(getLocalPlayer()))
				    local nitroCount = getVehicleNitroCount(getPedOccupiedVehicle(getLocalPlayer()))
		                if getVehicleUpgradeOnSlot(vehicle, 8) then
		                    if  nitro ~= false and nitro ~= nil and nitro > 0  then
                                dxDrawText("Nitro: "..nitroCount.." | "..math.floor(nitro/1*100).." %", 1150*sW, 695*sH, 1270*sW, 710*sH, tocolor(255, 255, 255, 255), 1.00, "default-bold", "left", "center", false, false, false, false, false)
                            else
                                dxDrawText("", 1150*sW, 695*sH, 1270*sW, 710*sH, tocolor(255, 255, 255, 255), 1.00, "default-bold", "left", "center", false, false, false, false, false)
			                end
				        end
			    end
		end)

--# Convert numbers
function convertNumber ( number )
    local formatted = number
    while true do
        formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
        if ( k==0 ) then
            break
        end
    end
    return formatted
end

addEventHandler( "onClientResourceStart", resourceRoot,
	function()
	    setPlayerHudComponentVisible ( "all", false )
	    setPlayerHudComponentVisible ( "radar", true )
	    setPlayerHudComponentVisible ( "crosshair", true )
	end);

addEventHandler( "onClientResourceStop", resourceRoot,
	function()
	    setPlayerHudComponentVisible ( "all", true )
	end)
